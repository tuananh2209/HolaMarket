package com.fu.jsclub.fumarket.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBContext {
	public Connection getConnection() throws Exception {
		String url = "jdbc:sqlserver://" + serverName + ":" + portNumber + ";databaseName=" + dbName;
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		return DriverManager.getConnection(url, userID, password);
	}

	/* Insert your other code right after this comment */

	/*
	 * Change/update information of your database connection, DO NOT change name of
	 * instance variables in this class
	 */
	private final String serverName = "localhost";
	private final String dbName = "CodingProject";
	private final String portNumber = "1433";
	private final String userID = "sa";
	private final String password = "anhtu2561998";
}
