package com.fu.jsclub.fumarket.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fu.jsclub.fumarket.DAO.ProductDAO;
import com.fu.jsclub.fumarket.model.ProductDetailModel;
import com.fu.jsclub.fumarket.model.Products;
import com.fu.jsclub.fumarket.model.SlideshowImage;

@Controller
public class HomepageController {

	@RequestMapping("/")
	public String home(@RequestParam(value = "categoryID", required = false, defaultValue = "0") int categoryID,
			@RequestParam(value = "page", required = false, defaultValue = "0") int page, HttpServletRequest request,
			Model model) throws Exception {
		if (Common.isLogin() && Common.isAdmin()) {
			return "homepageForAdmin";
		}
		
		List<ProductDetailModel> products = new ArrayList<ProductDetailModel>();
		ProductDAO productDAO = new ProductDAO();
		int totalProduct = productDAO.totalProduct();
		if (categoryID == 0)
			products = productDAO.getListProduct(page * 12 + 1, page * 12 + 12);
		else
			products = productDAO.getListProductByCategory(1, 12, categoryID);
		String path = request.getServletContext().getRealPath("/");
		System.out.println(path);
		StringBuilder tmp = new StringBuilder();
		tmp.append(path);
		tmp.append("\\WEB-INF\\view\\media\\Product\\");
		String urlInFrontend = "view\\media\\Product\\";
		for (int i = 0; i < products.size(); i++) {
			int pID = products.get(i).getProductID();
			File folder = new File(tmp.toString() + pID);
			File[] listOfFiles = folder.listFiles();
			if (listOfFiles != null && listOfFiles.length > 0) {
				products.get(i).setImageURL(urlInFrontend + pID + "\\" + listOfFiles[0].getName());
				System.out.println(products.get(i).getImageURL());
			}
		}
		model.addAttribute("totalProduct", totalProduct);
		model.addAttribute("categoryID", categoryID);
		model.addAttribute("page", page);
		model.addAttribute("products", products);
		return "home";
	}

}
