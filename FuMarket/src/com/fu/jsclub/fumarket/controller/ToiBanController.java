package com.fu.jsclub.fumarket.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ToiBanController {
	@RequestMapping("/Toiban")
	public String showToiban() {
		return "toiban";
	}
}
