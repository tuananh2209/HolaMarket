package com.fu.jsclub.fumarket.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fu.jsclub.fumarket.DAO.UserDAO;
import com.fu.jsclub.fumarket.model.Users;

@Controller
public class AccountController {
	
	@RequestMapping("/processLogin")
	@ResponseBody
	public String processLogin(@RequestParam("email") String email,  @RequestParam("password") String password, HttpSession session)
	{
		UserDAO userDAO = new UserDAO();
		Users u = userDAO.checkLogin(email, password);
		
		if (u == null) {
			return "FAIL";
		}
		
		session.setAttribute(Common.USER, u);
		return "SUCCESS";
	}
	
	
	@RequestMapping("/logout")
	public String logout(HttpSession session)
	{
		session.setAttribute(Common.USER, null);
		return "redirect:" +"/";
	}
	
	@RequestMapping("/accountManager")
	public String accountManager()
	{
		return "AccountManager";
	}
	
	@RequestMapping("/tmp")
	public String Order(HttpSession session, @RequestParam("productID") String productID, @RequestParam("quantity") String quantity)
	{
//		Object o = session.getAttribute("c");
//		System.out.println(o);
//		if (o == null)
//		{
//			return "login";
//		}
//		else {
//			
//			
//			List<Order> orders = (List<Order>)session.getAttribute("order");
//			
//			if (orders == null) {
//				
//				orders = new ArrayList<Order>();
//				
//			}
//			orders.add(new Order(productID, quantity));
//			
//			session.setAttribute("order", orders);
//		}
//		}
		return null;
	}
}
