package com.fu.jsclub.fumarket.controller;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fu.jsclub.fumarket.model.Users;

public class Common {
	
	public static final String USER = "USER";
	
	
	public static boolean isLogin()
	{
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		Object user = session.getAttribute(Common.USER);
		return user != null;
	}
	
	public static Users getCurrentUser()
	{
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		Object user = session.getAttribute(USER);
		return (Users) user;
	}
	
	public static String convertStrToUTF8(String s) {
		byte[] bytes = s.getBytes(StandardCharsets.ISO_8859_1);
		return new String(bytes, StandardCharsets.UTF_8);
	}
	
	public static boolean isAdmin()
	{
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession(true);
		Users user = (Users)session.getAttribute(Common.USER);
		return user.getRole() == 0;
	}
	
	public static String getImageUrlOfProduct(int productID)
	{
		
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = attr.getRequest();
		String path = request.getServletContext().getRealPath("/");
		StringBuilder tmp = new StringBuilder();
		tmp.append(path);
		tmp.append("\\WEB-INF\\view\\media\\Product\\");
		String urlInFrontend = "view\\media\\Product\\";
		
			File folder = new File(tmp.toString()+productID);
			File[] listOfFiles = folder.listFiles();
			if (listOfFiles != null && listOfFiles.length > 0) 
			{
				return urlInFrontend+productID+"\\"+listOfFiles[0].getName();
			}
		
		return null;
	}
	
}
