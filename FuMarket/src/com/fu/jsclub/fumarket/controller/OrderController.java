package com.fu.jsclub.fumarket.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fu.jsclub.fumarket.DAO.OrderDAO;
import com.fu.jsclub.fumarket.DAO.ProductDAO;
import com.fu.jsclub.fumarket.model.OrderDetail;
import com.fu.jsclub.fumarket.model.OrderViewModel;
import com.fu.jsclub.fumarket.model.Orders;
import com.fu.jsclub.fumarket.model.ProductDetailModel;
import com.fu.jsclub.fumarket.model.Products;

@Controller
public class OrderController {

	@RequestMapping("/ordernow")
	public String orderNow(@RequestParam("productID") int productID, @RequestParam("quantity") int quantity)
	{
		int userID = Common.getCurrentUser().getUserID();
		
		OrderDAO orderDAO = new OrderDAO();
		Date d = new Date();
		java.sql.Date sqlDate = new java.sql.Date(d.getTime());
		try {
			orderDAO.insertOrder(2, sqlDate, userID, 1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return null;
	}
	
	@RequestMapping("/addProductToCart")
	@ResponseBody
	public String addProductToCart(@RequestParam("productName") String productName, @RequestParam("sellerName") String sellerName, @RequestParam("price") int price,@RequestParam("productID") int productID, @RequestParam("quantity") int quantity, HttpSession session)
	{
		System.out.println(productID);
		System.out.println(productName);
		System.out.println(sellerName);
		System.out.println(quantity);
		System.out.println(price);
		
		List<OrderViewModel> cart = (List<OrderViewModel>)session.getAttribute("cart");
		if (cart == null) {
			cart = new ArrayList<OrderViewModel>();
		}
		cart.add(new OrderViewModel(productID,productName,quantity,price,sellerName));
		
		session.setAttribute("cart", cart);
		
		return "SUCCESS";
	}
	
	@RequestMapping("/cart")
	public String cart(HttpSession session)
	{
	
		
		return "cart";
	}
	
}
