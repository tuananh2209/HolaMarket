package com.fu.jsclub.fumarket.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fu.jsclub.fumarket.DAO.FeedbackDAO;
import com.fu.jsclub.fumarket.DAO.UserDAO;
import com.fu.jsclub.fumarket.model.Users;
import com.fu.jsclub.fumarket.model.Feedback_User;

@Controller
public class FeedbackController {

	@RequestMapping("/feedback")
	public String showFeedback(HttpServletRequest request, Model model)
	{
		System.out.println(Common.isLogin());
		if(!Common.isLogin()) return "redirect:" + "/";
		else {
			System.out.println(Common.isAdmin());
			System.out.println("1");
			if(Common.isAdmin()) {
		        List<Feedback_User> feedbacks = new ArrayList<Feedback_User>();
		        FeedbackDAO feedbackDAO = new FeedbackDAO();
		        feedbacks = feedbackDAO.getListFeedback(0, 6);

		        model.addAttribute("feedbacks", feedbacks);
		        if(feedbacks != null) {
		        	System.out.println(1);
		        }
		        	
		        
	
				return "manageFeedbackForAdmin";
			}
				
			else return "feedback";
		}
	}
	
	
	
	@RequestMapping("/getFeedback")
	@ResponseBody
	public String getFeedback(@RequestParam("content") String content, HttpSession session)
	{
		Users user = (Users)session.getAttribute(Common.USER);
		
		FeedbackDAO feedbackDAO = new FeedbackDAO();
		
		feedbackDAO.insertFeedback(user.getUserID(), content);
		return "SUCCESS";
		
	}
}
