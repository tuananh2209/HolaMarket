package com.fu.jsclub.fumarket.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fu.jsclub.fumarket.DAO.CategoryDAO;
import com.fu.jsclub.fumarket.DAO.FeedbackDAO;
import com.fu.jsclub.fumarket.DAO.ProductDAO;
import com.fu.jsclub.fumarket.DAO.RatingDAO;
import com.fu.jsclub.fumarket.DAO.UserDAO;
import com.fu.jsclub.fumarket.model.Category;
import com.fu.jsclub.fumarket.model.ProductDetailModel;
import com.fu.jsclub.fumarket.model.Products;
import com.fu.jsclub.fumarket.model.Rating;
import com.fu.jsclub.fumarket.model.SlideshowImage;
import com.fu.jsclub.fumarket.model.Users;

@Controller
public class ProductDetailController {
	@RequestMapping("/ProductDetail/{ProductID}")
	public String showProductDetail(HttpServletRequest request, @PathVariable("ProductID") String productID,
			Model model, HttpSession session) throws Exception {

		// Check product co ton tai hay ko
		String path = request.getServletContext().getRealPath("/");
//		System.out.println(path);
		StringBuilder tmp = new StringBuilder();
		tmp.append(path);
		tmp.append("\\WEB-INF\\view\\media\\Product\\");
		tmp.append(productID);

		// Read all file in path
		File folder = new File(tmp.toString());
		File[] listOfFiles = folder.listFiles();
		List<SlideshowImage> imgUrls = new ArrayList<>();
		int i = 1;
		for (File file : listOfFiles) {
			if (file.isFile()) {
				imgUrls.add(new SlideshowImage("/view/media/Product/" + productID.toString() + "/" + file.getName(),
						"myImg" + i++, file.getName()));
			}
		}
		model.addAttribute("imgs", imgUrls);
		
		//Show info product
		ProductDetailModel p = new ProductDAO().getInfoProduct(Integer.parseInt(productID));
		//get User Current
		Users user = (Users)session.getAttribute(Common.USER);
		
		if(p == null)
			return "home";
		else {
			model.addAttribute("u", user);
			model.addAttribute("p", p);
		}
		
		//Relative Product	
		Products product = new ProductDAO().getProductCategory(Integer.parseInt(productID));
		List<Products> pro = new ProductDAO().searchProductByCategory(0, 5, product.getCategoryID());
		
		for (Products products : pro) {
			products.setImageURL(Common.getImageUrlOfProduct(products.getProductID()));
		}
		
		model.addAttribute("rel", pro);
		return "ProductDetail";
	}
	@RequestMapping("/getRating/{productID}")
	@ResponseBody
	public String getFeedback(@RequestParam("content") String content,@PathVariable("productID") String productID, HttpSession session)
	{
		Users user = (Users)session.getAttribute(Common.USER);
		
		RatingDAO rate = new RatingDAO();
		try {
			rate.insertRating(content, 4, user.getUserID(), Integer.parseInt(productID));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "SUCCESS";
		
	}
	
}

