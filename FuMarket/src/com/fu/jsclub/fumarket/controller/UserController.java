package com.fu.jsclub.fumarket.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.fu.jsclub.fumarket.DAO.UserDAO;
import com.fu.jsclub.fumarket.model.Users;

@Controller
public class UserController {

	@RequestMapping("/processRegister")
	//@ResponseBody
	public String Register(@RequestParam("email") String email, @RequestParam("name") String name,
			@RequestParam("psw") String password, @RequestParam("phone") String phone,
			@RequestParam("gender") String gender, @RequestParam("address") String address, HttpSession session

	) throws Exception {
		UserDAO addUser = new UserDAO();
		int ID = addUser.insertUser(Common.convertStrToUTF8(name), email, password, address, gender, phone);
		Users u = new Users(Common.convertStrToUTF8(name), email, password, ID, address, gender, phone, 0, 1);

		session.setAttribute(Common.USER, u);

		return "redirect:" +"/";

	}

	@RequestMapping("/processUpdateProfile")
	public String Update(@RequestParam("email") String email, @RequestParam("name") String name,
			@RequestParam("psw") String password, @RequestParam("phone") String phone,
			@RequestParam("gender") String gender, @RequestParam("address") String address, HttpSession session

	) {

		System.out.println("Email " + email);
		System.out.println("Name " + name);
		System.out.println("pass " + password);
		System.out.println("Phone " + phone);
		System.out.println("Gender " + gender);
		System.out.println("Address " + address);

		Users currUser = Common.getCurrentUser();
		int ID = currUser.getUserID();
		System.out.println("ID: " + ID);
		UserDAO addUser = new UserDAO();
		try {
			addUser.updateUser(Common.convertStrToUTF8(name), email, password, address, gender, phone, ID);
			Users u = new Users(Common.convertStrToUTF8(name), email, password, ID, address, gender, phone, 0, 1);

			session.setAttribute(Common.USER, u);
		} catch (Exception e) {
			System.out.println("Smt wrong");
		}
		return "redirect:" +"/";

	}

}
