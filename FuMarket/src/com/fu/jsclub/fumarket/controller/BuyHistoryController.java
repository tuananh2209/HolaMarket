package com.fu.jsclub.fumarket.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BuyHistoryController {
	@RequestMapping("/buyHistory")
	public String showBuyHistory() {
		return "buyHistory";
	}
}
