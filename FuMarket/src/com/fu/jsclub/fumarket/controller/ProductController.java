package com.fu.jsclub.fumarket.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fu.jsclub.fumarket.DAO.ProductDAO;

@Controller
public class ProductController {

	@RequestMapping("/addProduct")
	public String showUploadProduct(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Content-Type", "text/html; charset=UTF-8");
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (!Common.isLogin()) {
			return "redirect:" + "/";
		}
		return "addProduct";
	}

	@RequestMapping("/processUploadProduct")
	public String processUploadProduct(@RequestParam("images") MultipartFile[] files,
			@RequestParam("productName") String productName, @RequestParam("price") float price,
			@RequestParam("quantity") int quantity, @RequestParam("category") int categoryID,
			@RequestParam("description") String description, HttpServletRequest request) {
	
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		byte[] bytes = productName.getBytes(StandardCharsets.ISO_8859_1);
		productName = new String(bytes, StandardCharsets.UTF_8);
		
		
		bytes = description.getBytes(StandardCharsets.ISO_8859_1);
		description = new String(bytes, StandardCharsets.UTF_8);
		ProductDAO p = new ProductDAO();
		int productID = -1;
		try {
			productID = p.insertProduct(productName, quantity, price, Common.getCurrentUser().getUserID(), categoryID,
					description);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// insert image to folder

		String path = request.getServletContext().getRealPath("/");
		
		StringBuilder productPath = new StringBuilder();
		productPath.append(path);
		productPath.append("WEB-INF\\view\\media\\Product\\");
		MkDirIfnExist(productPath.toString());
		productPath.append(Integer.toString(productID));
		MkDirIfnExist(productPath.toString());

		for (MultipartFile file : files) {
			String filePath = productPath+ "\\" + file.getOriginalFilename();
			try {
				file.transferTo(new File(filePath));
			} catch (IllegalStateException e) {	
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println(productPath);
		return "redirect:"+"ProductDetail/"+productID;
	}

	public void MkDirIfnExist(String path) {
		if (!new File(path).exists()) {
			new File(path).mkdir();
		}
	}

}
