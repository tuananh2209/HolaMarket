package com.fu.jsclub.fumarket.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.fu.jsclub.fumarket.db.DBContext;
import com.fu.jsclub.fumarket.model.ProductDetailModel;
import com.fu.jsclub.fumarket.model.Products;
import com.fu.jsclub.fumarket.model.Users;

public class ProductDAO {
	public int totalProduct() {
		try {
			Connection conn = new DBContext().getConnection();
			String sql = "select COUNT(*)  from Products";

			PreparedStatement psmt = conn.prepareStatement(sql);

			ResultSet rs = psmt.executeQuery();

			while (rs.next()) {
				return rs.getInt(1);

			}
			conn.close();
		} catch (SQLException se) {
			System.out.println(se);
		} catch (Exception e) {
			System.out.println(e);
		}
		return 0;
	}

	public int countProductByCategoryID(int categoryID) {
		try {
			Connection conn = new DBContext().getConnection();
			String sql = "select COUNT(*)  from Products where CategoryID =  " + categoryID;

			PreparedStatement psmt = conn.prepareStatement(sql);

			ResultSet rs = psmt.executeQuery();

			while (rs.next()) {
				return rs.getInt(1);
			}
			conn.close();
		} catch (SQLException se) {
			System.out.println(se);
		} catch (Exception e) {
			System.out.println(e);
		}
		return 0;
	}

	public ProductDetailModel getInfoProduct(int productID) {
		try {
			Connection conn = new DBContext().getConnection();
			String sql = "SELECT   Products.ProductName, Products.Price, Products.Quantity, Products.Rate, Users.FullName, Category.CategoryName,Products.description\r\n"
					+ "FROM   Products, Category, Users\r\n"
					+ "where Products.UserID = Users.UserID and Category.CategoryID = Products.CategoryID and ProductID = ? and Products.Ban = 0 and active = 1";
			PreparedStatement psmt = conn.prepareStatement(sql);
			psmt.setInt(1, productID);
			ResultSet rs = psmt.executeQuery();
//			List<ProductDetailModel> pdm = new ArrayList<>();
			while (rs.next()) {
				return new ProductDetailModel(productID,rs.getString(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4),
						rs.getString(5), rs.getString(6), rs.getString(7));

			}
			conn.close();
		} catch (SQLException se) {
			System.out.println(se);
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	public Products getProductCategory(int productID) {
		try {
			Connection conn = new DBContext().getConnection();
			String sql = "SELECT * from Products where productID = ? and Products.Ban = 0 and active = 1";
			PreparedStatement psmt = conn.prepareStatement(sql);
			psmt.setInt(1, productID);
			ResultSet rs = psmt.executeQuery();
//			List<ProductDetailModel> pdm = new ArrayList<>();
			while (rs.next()) {
				return new Products(rs.getString(1), productID, rs.getInt(3), rs.getDouble(4), rs.getDouble(5), rs.getInt(6), rs.getInt(7), 0, 1, rs.getString(8));

			}
			conn.close();
		} catch (SQLException se) {
			System.out.println(se);
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
	public List<ProductDetailModel> getListProduct(int beginIndex, int endIndex) throws Exception {

		List<ProductDetailModel> Products = new ArrayList<>();
		String query = "select e.ProductID, e.ProductName, e.Price, e.Quantity, e.Rate, e.FullName, e.CategoryName, e.Description from\r\n"
				+ "(select ROW_NUMBER() OVER (ORDER BY c.ProductID) as RowNum,c.*,b.CategoryName,a.FullName from Users a, Category b, Products c \r\n"
				+ "where a.UserID = c.UserID and b.CategoryID = c.CategoryID) as e \r\n" + "where e.RowNum between "
				+ beginIndex + " and " + endIndex + " and ban =0 ";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int productID = rs.getInt(1);
			String productName = rs.getString(2);
			
			if (productName.length() > 27) {
				productName = productName.substring(0, 26) + "...";
			}
			int price = rs.getInt(3);
			int quantity = rs.getInt(4);
			float rate = rs.getFloat(5);
			String fullName = rs.getString(6);
			String categoryName = rs.getString(7);
			String description = rs.getString(8);
			Products.add(new ProductDetailModel(productID, productName, price, quantity, rate, fullName, categoryName,
					description));
		}
		ps.close();
		conn.close();
		return Products;
	}

	public List<ProductDetailModel> getListProductByCategory(int beginIndex, int endIndex, int categoryid)
			throws Exception {

		List<ProductDetailModel> Products = new ArrayList<>();
		String query = "select e.ProductID, e.ProductName, e.Price, e.Quantity, e.Rate, e.FullName, e.CategoryName, e.Description from\r\n"
				+ "(select ROW_NUMBER() OVER (ORDER BY c.ProductID) as RowNum,c.*,b.CategoryName,a.FullName from Users a, Category b, Products c \r\n"
				+ "where a.UserID = c.UserID and b.CategoryID = c.CategoryID and c.CategoryID = " + categoryid
				+ ") as e \r\n" + "where e.RowNum between " + beginIndex + " and " + endIndex + " and ban =0 ";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int productID = rs.getInt(1);
			String productName = rs.getString(2);
			int price = rs.getInt(3);
			int quantity = rs.getInt(4);
			float rate = rs.getFloat(5);
			String fullName = rs.getString(6);
			String categoryName = rs.getString(7);
			String description = rs.getString(8);
			Products.add(new ProductDetailModel(productID, productName, price, quantity, rate, fullName, categoryName,
					description));
		}
		ps.close();
		conn.close();
		return Products;
	}

	public List<ProductDetailModel> getListProductByName(int beginIndex, int endIndex, String name)
			throws Exception {

		List<ProductDetailModel> Products = new ArrayList<>();
		String query = "select e.ProductID, e.ProductName, e.Price, e.Quantity, e.Rate, e.FullName, e.CategoryName, e.Description from\r\n"
				+ "(select ROW_NUMBER() OVER (ORDER BY c.ProductID) as RowNum,c.*,b.CategoryName,a.FullName from Users a, Category b, Products c \r\n"
				+ "where a.UserID = c.UserID and b.CategoryID = c.CategoryID and c.ProductName like '%" + name
				+ "%'"  
				+ ") as e \r\n" + "where e.RowNum between " + beginIndex + " and " + endIndex + " and ban =0 ";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int productID = rs.getInt(1);
			String productName = rs.getString(2);
			int price = rs.getInt(3);
			int quantity = rs.getInt(4);
			float rate = rs.getFloat(5);
			String fullName = rs.getString(6);
			String categoryName = rs.getString(7);
			String description = rs.getString(8);
			Products.add(new ProductDetailModel(productID, productName, price, quantity, rate, fullName, categoryName,
					description));
		}
		ps.close();
		conn.close();
		return Products;
	}
	public List<Products> searchProductByCategory(int beginIndex, int endIndex, int categoryid) throws Exception {

		List<Products> Products = new ArrayList<>();
		String query = "select e.ProductID,e.ProductName,e.Price, e.CategoryID, e.Quantity,e.Rate,e.UserID from \r\n"
				+ "(select ROW_NUMBER() OVER (ORDER BY c.ProductID) as RowNum,c.* from Users a, Category b, Products c \r\n"
				+ "where a.UserID = c.UserID and b.CategoryID = c.CategoryID and b.categoryID = " + categoryid
				+ ") as e \r\n" + "where e.RowNum between " + beginIndex + " and " + endIndex + " and ban =0";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int productID = rs.getInt(1);
			String productName = rs.getString(2);
			if(productName.length() >9) {
				productName = productName.substring(0, 8) + "...";
			}
			Float price = rs.getFloat(3);
			int categoryID = rs.getInt(4);
			int quantity = rs.getInt(5);
			float rate = rs.getFloat(6);
			int userID = rs.getInt(7);
			Products.add(new Products(productID, productName, price, categoryID, quantity, rate, userID));
		}
		ps.close();
		conn.close();
		return Products;
	}

	public List<Products> searchProductByName(int beginIndex, int endIndex, String name) throws Exception {

		List<Products> Products = new ArrayList<>();
		String query = "select e.ProductID,e.ProductName,e.Price, e.CategoryID, e.Quantity,e.Rate,e.UserID from \r\n"
				+ "(select ROW_NUMBER() OVER (ORDER BY c.ProductID) as RowNum,c.* from Users a, Category b, Products c \r\n"
				+ "where a.UserID = c.UserID and b.CategoryID = c.CategoryID and c.ProductName like '% " + name + "%'"
				+ ") as e \r\n" + "where e.RowNum between " + beginIndex + " and " + endIndex + " and ban =0";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int productID = rs.getInt(1);
			String productName = rs.getString(2);
			Float price = rs.getFloat(3);
			int categoryID = rs.getInt(4);
			int quantity = rs.getInt(5);
			float rate = rs.getFloat(6);
			int userID = rs.getInt(7);
			Products.add(new Products(productID, productName, price, categoryID, quantity, rate, userID));
		}
		ps.close();
		conn.close();
		return Products;
	}

	public void updateQuantity(int quantity, int productID) throws Exception {
		String query = "update Products set Quantity = ? where ProductID = ?";

		try {
			Connection conn = new DBContext().getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setInt(1, quantity);
			ps.setInt(2, productID);
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void activeProduct(int productID) throws Exception {
		String query = "update Products set Active = 1 where ProductID = 1";

		try {
			Connection conn = new DBContext().getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setInt(1, productID);
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void inactiveProduct(int productID) throws Exception {
		String query = "update Products set Active = 0 where ProductID = 1";

		try {
			Connection conn = new DBContext().getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setInt(1, productID);
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteProduct(int productID) throws Exception {
		String query = "update Products set ban = 1 where productID = ?";

		try {
			Connection conn = new DBContext().getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setInt(1, productID);
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Products> searchProductByUserID(int id) throws Exception {

		List<Products> Products = new ArrayList<>();
		String query = "select ProductID, ProductName, Price, CategoryID, Quantity, Rate, UserID from Products where UserID = "
				+ id + " and ban = 0";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int productID = rs.getInt(1);
			String productName = rs.getString(2);
			Float price = rs.getFloat(3);
			int categoryID = rs.getInt(4);
			int quantity = rs.getInt(5);
			float rate = rs.getFloat(6);
			int userID = rs.getInt(7);
			Products.add(new Products(productID, productName, price, categoryID, quantity, rate, userID));
		}
		ps.close();
		conn.close();
		return Products;
	}

	public int insertProduct(String productName, int quantity, float price, int userID, int categoryID,
			String description) throws Exception {
		int productID = -1;
		String query = "INSERT INTO Products OUTPUT INSERTED.ProductID VALUES(?,?,?,?,?,?,?,?,?)";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		System.out.println(productName);
		// set param
		ps.setNString(1, productName);
		ps.setInt(2, quantity);
		ps.setFloat(3, price);
		ps.setInt(4, 0);
		ps.setInt(5, userID);
		ps.setInt(6, categoryID);
		ps.setInt(7, 0);
		ps.setInt(8, 1);
		ps.setNString(9, description);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			productID = rs.getInt(1);
		}
		ps.close();
		conn.close();
		return productID;
	}
}
