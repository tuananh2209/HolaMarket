package com.fu.jsclub.fumarket.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.fu.jsclub.fumarket.db.DBContext;
import com.fu.jsclub.fumarket.model.Users;

public class UserDAO {
	public List<Users> getListUser(int beginIndex, int endIndex) throws Exception {

		List<Users> users = new ArrayList<>();
		String query = "select a.FullName, a.UserID, a.Email , a.Phone  from \r\n"
				+ "(select *,  ROW_NUMBER() OVER (ORDER BY UserID) as RowNum from Users) as a \r\n"
				+ "where a.RowNum between " + beginIndex + " and " + endIndex + " and a.Role = 1 and a.ban = 0";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String fullname = rs.getString(1);
			int userID = rs.getInt(2);
			String email = rs.getString(3);
			String phone = rs.getString(4);
			users.add(new Users(fullname, userID, email, phone));
		}
		ps.close();
		conn.close();
		return users;
	}

	public List<Users> searchUserByName(int beginIndex, int endIndex, String name) throws Exception {

		List<Users> users = new ArrayList<>();
		String query = "select a.FullName, a.UserID, a.Email , a.Phone  from \r\n"
				+ "(select *,  ROW_NUMBER() OVER (ORDER BY UserID) as RowNum " + "from Users b where b.Fullname like '%"
				+ name + "%' ) as a \r\n" + "where a.RowNum between " + beginIndex + " and " + endIndex
				+ " and a.Role = 1 and a.ban =0";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			String fullname = rs.getString(1);
			int userID = rs.getInt(2);
			String email = rs.getString(3);
			String phone = rs.getString(4);
			users.add(new Users(fullname, userID, email, phone));
		}
		ps.close();
		conn.close();
		return users;
	}

	public int insertUser(String fullname, String email, String password, String address, String gender, String phone)
			throws Exception {
		String query = "INSERT INTO Users OUTPUT inserted.UserID VALUES(?,?,?,?,?,?,?,?) ";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setNString(1, fullname);
		ps.setString(2, email);
		ps.setString(3, password);
		ps.setString(4, address);
		ps.setString(5, gender);
		ps.setString(6, phone);
		ps.setInt(7, 1);
		ps.setInt(8, 0);
		//ps.executeUpdate();
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			
			int userID = rs.getInt(1);
			return userID;
			
		}
		ps.close();
		conn.close();
		return 0;
	}

	public void deleteUserById(int id) throws Exception {
		String query = "update Users set ban = 1 where userID = ?";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setInt(1, id);
		ps.executeUpdate();
		ps.close();
		conn.close();
	}

	public Users getUserById(int id) {

		try {
			Connection conn = new DBContext().getConnection();
			String sql = "Select * from Users where UserID = ? ";
			PreparedStatement psmt = conn.prepareStatement(sql);
			psmt.setInt(1, id);
			ResultSet rs = psmt.executeQuery();
			while (rs.next()) {
				Users t = new Users(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5),
						rs.getString(6), rs.getString(7), rs.getInt(8), rs.getInt(9));
				return t;
			}
			conn.close();
		} catch (SQLException se) {
			System.out.println(se);
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public Users checkLogin(String email, String password) {

		try {
			Connection conn = new DBContext().getConnection();
			String sql = "Select * from Users where email = ? and password = ?";
			PreparedStatement psmt = conn.prepareStatement(sql);
			psmt.setString(1, email);
			psmt.setString(2, password);
			ResultSet rs = psmt.executeQuery();
			while (rs.next()) {
				Users t = new Users(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getString(5),
						rs.getString(6), rs.getString(7), rs.getInt(8), rs.getInt(9));
				return t;
			}
			conn.close();
		} catch (SQLException se) {
			System.out.println(se);
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

//	public Users checkLogin(String email, String password) {
//		return getUser(email, password);
//	}

	public void updateUser(String fullname, String email, String password, String address, String gender, String phone,
			int id) throws Exception {
		String query = "update Users set FullName = ?, email = ?, Password = ?,"
				+ "	Address = ?, Gender = ?, Phone = ?" + "	where UserID = ?";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setNString(1, fullname);
		ps.setString(2, email);
		ps.setString(3, password);
		ps.setString(4, address);
		ps.setString(5, gender);
		ps.setString(6, phone);
		ps.setInt(7, id);
		
		ps.executeUpdate();
		ps.close();
		conn.close();
	}

	public void changeRoom(String address, int userID) throws Exception {
		String query = "update Users set Address = ? where UserID = ? ";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setString(1, address);
		ps.setInt(2, userID);
		ps.executeUpdate();
		ps.close();
		conn.close();
	}

}
