package com.fu.jsclub.fumarket.DAO;


import java.sql.Connection;
import java.sql.PreparedStatement;


import com.fu.jsclub.fumarket.db.DBContext;
public class RatingDAO {
	public void insertRating(String commend, float rate, int uID, int pID) throws Exception {
		String query = "INSERT INTO Rating VALUES(?,?,?,?)";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setString(1, commend);
		ps.setFloat(2, rate);
		ps.setInt(3, uID);
		ps.setInt(4, pID);

		ps.executeUpdate();
		ps.close();
		conn.close();
	}

}
