package com.fu.jsclub.fumarket.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.fu.jsclub.fumarket.db.DBContext;

public class OrderDAO {
	public void insertOrder(int status, Date date, int userID, int active) throws Exception {
		String query = "INSERT INTO Orders  OUTPUT inserted.OrderID VALUES(?,?,?,?)";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setInt(1, status);
		ps.setDate(2, date);
		ps.setInt(3, userID);
		ps.setInt(4, active);
		ResultSet rs = ps.executeQuery();
		while (rs.next())
		{
			// todo
		}
		ps.close();
		conn.close();
	}

	public void updateStatusConfirm(int orderID) throws Exception {
		String query = "update Orders set Status = 1 where OrderID = ? ";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
	
		ps.setInt(1, orderID);
		ps.executeUpdate();
		ps.close();
		conn.close();
	}
	public void updateStatusRefuse(int orderID) throws Exception {
		String query = "update Orders set Status = 3 where OrderID = ? ";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
	
		ps.setInt(1, orderID);
		ps.executeUpdate();
		ps.close();
		conn.close();
	}
	
}
