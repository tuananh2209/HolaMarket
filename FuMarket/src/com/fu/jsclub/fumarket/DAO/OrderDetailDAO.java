package com.fu.jsclub.fumarket.DAO;


import java.sql.Connection;
import java.sql.PreparedStatement;


import com.fu.jsclub.fumarket.db.DBContext;

public class OrderDetailDAO {
	public void insertOrderDetail(int productID, int orderID, int quantity) throws Exception {
		String query = "insert into OrderDetail(ProductID,OrderID,Quantity) values (?,?,?)";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setInt(1, productID);
		ps.setInt(2, orderID);
		ps.setInt(3, quantity);

		ps.executeUpdate();
		ps.close();
		conn.close();
	}
}
