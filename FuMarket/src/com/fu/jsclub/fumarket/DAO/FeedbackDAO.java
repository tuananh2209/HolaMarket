package com.fu.jsclub.fumarket.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.fu.jsclub.fumarket.db.DBContext;
import com.fu.jsclub.fumarket.model.Feedback_User;
import com.fu.jsclub.fumarket.model.Products;


public class FeedbackDAO {
	public void insertFeedback(int userID, String content) {
		try {
		String query = "INSERT INTO Feedback VALUES(?,?)";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setInt(1, userID);
		ps.setString(2, content);

		ps.executeUpdate();
		ps.close();
		conn.close();
		}catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void updateFeedback(int userID, String content) throws Exception {
		String query = "update Feedback set Feedback = ? where userID = ?";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		// set param
		ps.setString(1, content);
		ps.setInt(2, userID);

		ps.executeUpdate();
		ps.close();
		conn.close();
	}
	
	
	public List<Feedback_User> getListFeedback(int beginIndex, int endIndex){
		List<Feedback_User> feedbacks = new ArrayList<>();
		try {
		String query = "select feedbackID,FullName,UserID,feedback from (" + 
				"select ROW_NUMBER() OVER (ORDER BY f.FeedbackID) as RowNum,f.FeedbackID,u.*,f.FeedBack from Feedback f, Users u " + 
				"where f.UserID = u.UserID ) as e where e.RowNum between " +  beginIndex + " and "  + endIndex;
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int feedbackID = rs.getInt(1);
			String userName = rs.getString(2);
			int userID = rs.getInt(3);
			String content = rs.getString(4);
			Feedback_User feedback = new Feedback_User(feedbackID, userID, content, userName);
			feedbacks.add(feedback);
		}
		ps.close();
		conn.close();
		return feedbacks;
		} catch (Exception e) {
			System.out.println("Smt wrong");
		}
		return null;
	}

}
