package com.fu.jsclub.fumarket.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.fu.jsclub.fumarket.db.DBContext;
import com.fu.jsclub.fumarket.model.Category;
import com.fu.jsclub.fumarket.model.Category;


public class CategoryDAO {
	public List<Category> getListCategory() throws Exception {
		List<Category> Category = new ArrayList<>();
		String query = "select * from Category";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {

			int id = rs.getInt(1);
			String name = rs.getString(2);
			Category.add(new Category(id, name));
		}
		ps.close();
		conn.close();
		return Category;
	}
	public List<Category> searchCategoryByName( String name) throws Exception {

		List<Category> Category = new ArrayList<>();
		String query = "select * from Category where CategoryName like '%" + name
				+ "%'";
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {

			int id = rs.getInt(1);
			
			Category.add(new Category(id, name));
		}
		ps.close();
		conn.close();
		return Category;
	}
	public List<Category> searchCategoryById( int id) throws Exception {

		List<Category> Category = new ArrayList<>();
		String query = "select * from Category where CategoryID =  "+ id;
		Connection conn = new DBContext().getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {

			String name = rs.getString(2);
			
			Category.add(new Category(id, name));
		}
		ps.close();
		conn.close();
		return Category;
	}
}
