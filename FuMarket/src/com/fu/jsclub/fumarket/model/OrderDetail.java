package com.fu.jsclub.fumarket.model;

public class OrderDetail {

    private int orderDetailID;
    private int productID;
    private int orderID;
    private int quantity;

    public OrderDetail() {
    }

    public OrderDetail(int orderDetailID, int productID, int orderID, int quantity) {
        this.orderDetailID = orderDetailID;
        this.productID = productID;
        this.orderID = orderID;
        this.quantity = quantity;
    }
    
    public OrderDetail(int productID, int quantity) {
        this.productID = productID;
        this.quantity = quantity;
    }

    public int getOrderDetailID() {
        return orderDetailID;
    }

    public void setOrderDetailID(int orderDetailID) {
        this.orderDetailID = orderDetailID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
