package com.fu.jsclub.fumarket.model;

public class Feedback {
	private int feedbackID;
    private int userID;
    private String feedback;

    public Feedback() {
    }

    public Feedback(int feedbackID, int userID, String feedback) {
        this.feedbackID = feedbackID;
    	this.userID = userID;
        this.feedback = feedback;
    }
    public int getFeedbackID() {
		return feedbackID;
	}
    public void setFeedbackID(int feedbackID) {
		this.feedbackID = feedbackID;
	}
    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

}
