package com.fu.jsclub.fumarket.model;

import java.util.Date;

public class Orders {
    private int status;
    private int orderID;
    private Date date;
    private int userID;
    private int active;

    public Orders() {
    }

    public Orders(int status, int orderID, Date date, int userID, int active) {
        this.status = status;
        this.orderID = orderID;
        this.date = date;
        this.userID = userID;
        this.active = active;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int isActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }
    
}
