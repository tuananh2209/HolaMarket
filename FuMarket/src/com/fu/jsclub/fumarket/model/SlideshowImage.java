package com.fu.jsclub.fumarket.model;

public class SlideshowImage {
	private String URL;
	private String ID;
	private String name;

	public SlideshowImage(String url, String id, String name) {
		URL = url;
		ID = id;
		this.name = name;
	}

	public String getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public String getURL() {
		return URL;
	}

	public void setID(String id) {
		ID = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setURL(String url) {
		URL = url;
	}
}
