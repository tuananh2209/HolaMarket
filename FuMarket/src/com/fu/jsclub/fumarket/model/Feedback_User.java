package com.fu.jsclub.fumarket.model;

public class Feedback_User {

    private int userID,feedbackID;
    private String feedback, userName;

    public Feedback_User() {
    }

    public Feedback_User(int feedbackID,int userID, String feedback, String userName) {
    	this.feedbackID = feedbackID;
        this.userID = userID;
        this.feedback = feedback;
        this.userName = userName;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
    
    public int getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(int userID) {
        this.feedbackID = feedbackID;
    }


    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String feedback) {
    	this.userName = userName;
    }
    
}
