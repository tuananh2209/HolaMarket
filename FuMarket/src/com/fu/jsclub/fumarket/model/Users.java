package com.fu.jsclub.fumarket.model;
public class Users {

    private String fullName;
    private String email;
    private String password;
    private int userID;
    private String address;
    private String gender;
    private String phone;
    private int role;
    private int ban;

    public Users() {
    }

    public Users(String fullName, String email, String password, int userID, String address, String gender, String phone, int role, int ban) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.userID = userID;
        this.address = address;
        this.gender = gender;
        this.phone = phone;
        this.role = role;
        this.ban = ban;
    }
    public Users(String fullname, int userID, String email, String phone) {
    	 this.fullName = fullName;
         this.email = email;
         this.userID = userID;
         this.phone = phone;
    }
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getBan() {
        return ban;
    }

    public void setBan(int ban) {
        this.ban = ban;
    }

}
