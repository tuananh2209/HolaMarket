package com.fu.jsclub.fumarket.model;

public class Products {

    private String productName;
    private int productID;
    private int quantity;
    private double price;
    private double rate;
    private int userID;
    private int categoryID;
    private int ban;
    private int active;
    private String description;
    private String imageURL;

    public Products() {
    }
    public Products(int productID,String productName, float price, int categoryID, int quantity, float rate, int userID) {
    	this.productID = productID;
    	this.productName = productName;
    	 this.quantity = quantity;
         this.price = price;
         this.rate = rate;
         this.userID = userID;
         this.categoryID = categoryID;
    }
    public Products(String productName, int productID, int quantity, double price, double rate, int userID, int categoryID, int ban, int active, String description) {
        this.productName = productName;
        this.productID = productID;
        this.quantity = quantity;
        this.price = price;
        this.rate = rate;
        this.userID = userID;
        this.categoryID = categoryID;
        this.ban = ban;
        this.active = active;
        this.description = description;
    }

    public String getImageURL() {
		return imageURL;
	}
    
    public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
    
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public int isBan() {
        return ban;
    }

    public void setBan(int ban) {
        this.ban = ban;
    }

    public int isActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
