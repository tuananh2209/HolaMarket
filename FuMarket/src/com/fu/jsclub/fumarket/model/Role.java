package com.fu.jsclub.fumarket.model;
public class Role {

    private int roleID;
    private String nameRole;

    public Role() {
    }

    public Role(int roleID, String nameRole) {
        this.roleID = roleID;
        this.nameRole = nameRole;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }

}
