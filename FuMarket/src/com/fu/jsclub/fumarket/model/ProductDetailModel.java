package com.fu.jsclub.fumarket.model;

public class ProductDetailModel {
	private int productID;
	private String productName;
	private int price;
	private int quantity;
	private double rate;
	private String fullName;
	private String categoryName;
	private String description;
	private String imageURL;

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public ProductDetailModel() {
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public ProductDetailModel(int productID, String productName, int price, int quantity, double rate, String fullName,
			String categoryName, String description) {
		this.productID = productID;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
		this.rate = rate;
		this.fullName = fullName;
		this.categoryName = categoryName;
		this.description = description;
	}

	public ProductDetailModel(String productName, int price, int quantity, double rate, String fullName,
			String categoryName, String description) {
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
		this.rate = rate;
		this.fullName = fullName;
		this.categoryName = categoryName;
		this.description = description;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
