package com.fu.jsclub.fumarket.model;

public class OrderViewModel {
	private int productID;
	private String productName;
	private int quantity;
	private int price;
	private String sellerName;
	private int sellerID;
	
	
	public OrderViewModel(int productID, String productName, int quantity, int price, String sellerName) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.quantity = quantity;
		this.price = price;
		this.sellerName = sellerName;
	}
	public OrderViewModel(int productID, int quantity) {
		
		this.productID = productID;
		this.quantity = quantity;
	}
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public int getSellerID() {
		return sellerID;
	}
	public void setSellerID(int sellerID) {
		this.sellerID = sellerID;
	}
	
	
}
