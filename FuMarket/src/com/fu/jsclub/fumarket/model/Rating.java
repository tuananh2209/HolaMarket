package com.fu.jsclub.fumarket.model;

public class Rating {

    private String comment;
    private double rate;
    private int userID;
    private int productID;

    public Rating() {
    }

    public Rating(String comment, double rate, int userID, int productID) {
        this.comment = comment;
        this.rate = rate;
        this.userID = userID;
        this.productID = productID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

}
