<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- footer -->
	<footer id="myFooter">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<h2 class="logo">
						<a href="/FuMarket"> <img class="Logo5"
							src="<c:url value="view/media/Logo.jpg" />" alt="">
						</a>
					</h2>
				</div>
				<div class="col-sm-3">
					<h5>5 Chú ve sầu</h5>
					<ul>
						<li><a href="https://www.facebook.com/beovuive1"
							target="_blank">Nguyễn Quang Trường</a></li>
						<li><a href="https://www.facebook.com/t0ank55"
							target="_blank">Bùi Anh Tú</a></li>
						<li><a href="https://www.facebook.com/buithanhtung.317"
							target="_blank">Bùi Thanh Tùng</a></li>
						<li><a href="https://www.facebook.com/dovan.tuan.777"
							target="_blank">Đỗ Văn Tuấn</a></li>
						<li><a href="https://www.facebook.com/dangkhoaydl"
							target="_blank">Tạ Đăng Khoa</a></li>

						<li><a href="https://www.facebook.com/tuananh2209"
							target="_blank">Mentor: Nguyễn Tiến Tuấn Anh</a></li>

					</ul>
				</div>
				<div class="col-sm-2">
					<h5>About</h5>
					<ul>
						<li><a
							href="https://www.facebook.com/groups/fuhoalac/?fb_dtsg_ag=Adz97XB_Or08F-Vku6hzHGACzYQqBgSIEVh_OTMJlmudvQ%3AAdy_LghnAOnEIo249l_giNKSZvT5gWWiu45fzQ8AK4XHcg"
							target="_blank">Idea</a></li>
						<li><a href="https://gitlab.com/tuananh2209/HolaMarket.git"
							target="_blank">Our Open Source Code</a></li>

					</ul>
				</div>
				<div class="col-sm-2">
					<h5>License</h5>
					<ul>
						<li><a href="/">Home</a></li>

						<li><a href="#">Safety</a></li>
					</ul>
				</div>
				<div class="col-sm-2">
					<div class="social-networks">
						<a href="https://twitter.com/?lang=vi" class="twitter"> <i class=""> <img
								src="<c:url value= "view/media/icons8-twitter-64.png"/>" alt=""
								width="30px" height="30px">
						</i>
						</a> <a href="https://www.facebook.com/" class="facebook"> <i class=""> <img
								src="<c:url value="view/media/icons8-facebook-48.png"/>" alt=""
								width="30px" height="30px">
						</i>
						</a> <a href="https://www.youtube.com/" class="youtube"> <i class=""> <img
								src="<c:url value="view/media/youtube.png" />" alt=""
								width="30px" height="30px">
						</i>
						</a>
					</div>
					<a href="feedback">
						<button type="button" class="btn btn-default">Feedback</button>
					</a>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<p>©2018 Bản quyền thuộc về Team 5 chú ve sầu</p>
		</div>

	</footer>


	<script src="main.js"></script>
