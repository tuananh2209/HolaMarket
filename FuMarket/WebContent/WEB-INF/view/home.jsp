<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqyjobZKV_bef9qMkQw69brKBHJO8Xtho&libraries=places"
	type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<link rel="stylesheet" href="<c:url value="view/bootstrap.min.css"/>">

<script type="text/javascript"
	src="<c:url value="bootstrap-4.0.0-dist/js/bootstrap.min.js"/>"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="view/style.css"/>">
<link rel="icon" href="<c:url value="view/media/Icons/homepage.png" />"
	type="image/x-icon">

</head>

<title>Trang chủ</title>
</head>

<body>

	<%@ include file="header.jsp"%>

	<div class="container" style="margin-top: 5px;">
		<div class="cate">

			<ul>
				<li><a href="/FuMarket/?categoryID=2"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-soda-50.png" />" alt="">
					</span> <span> &nbspĐồ uống nhanh </span>
				</a></li>
				<li><a href="/FuMarket/?categoryID=3"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-food-50.png" />" alt="">
					</span> <span> &nbspĐồ ăn vặt </span>
				</a></li>
				<li><a href="/FuMarket/?categoryID=5"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-clothes-50.png" />" alt="">
					</span> <span> &nbspQuần áo </span>
				</a></li>
				<li><a href="/FuMarket/?categoryID=4"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-e-learning-64.png" />"
							alt="">
					</span> <span> &nbspĐồ công nghệ </span>
				</a></li>
				<li><a href="/FuMarket/?categoryID=11"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-iron-50.png" />" alt="">
					</span> <span> &nbspDịch vụ giặt là </span>
				</a></li>
				<li><a href="/FuMarket/?categoryID=6"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-water-50.png" />" alt="">
					</span> <span> &nbspShip nước </span>
				</a></li>
				<li><a href="/FuMarket/?categoryID=8"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-bank-cards-64.png" />"
							alt="">
					</span> <span> &nbspThẻ điện thoại </span>
				</a></li>
				<li><a href="/FuMarket/?categoryID=10"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-birthday-cake-50.png" />"
							alt="">
					</span> <span> &nbspĐặt bánh sinh nhật </span>
				</a></li>
				<li><a href="/FuMarket/?categoryID=1"> <span
						class="icon-wrap"> <img
							src="<c:url value="view/media/icons8-rice-bowl-64.png" />" alt="">
					</span> <span> &nbspĐặt cơm </span>
				</a></li>

			</ul>


		</div>

		<div class="slideshow">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<img src="<c:url value="view/media/Slide photo/5.jpg" />"
							alt="Los Angeles" height="500px">
					</div>

					<div class="item">
						<img src="<c:url value="view/media/Slide photo/6.jpg" />">
					</div>

					<div class="item ">
						<img src="<c:url value="view/media/Slide photo/7.jpg " />">
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="container" style="margin-top: 10px;">
		<div class="row">
			<c:forEach items="${products}" var="product">
				<div class="col-sm-3 cach">
					<div class="card">
						<img class="card-img-top" src="${product.imageURL }" alt="">
						<div class="card-body">
							<h5 class="card-title">${product.productName}</h5>
							<h4>${product.price}</h4>
							<p class="card-text">
							<p>Danh mục: ${product.categoryName}</p>
							<p>Số lượng: ${product.quantity}</p>
							<p>Đánh giá: ${product.rate} sao</p>
							<p>người bán: ${product.fullName}</p>
							</p>
							<a href="/FuMarket/ProductDetail/${product.productID }"
								class="btn btn-primary">Xem chi tiết</a>
						</div>
					</div>
				</div>
			</c:forEach>

		</div>
		<div class="row" style="height: 50px">
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination" style="margin: 8px; float: right;">
					<c:if test="${categoryID == 0 }">
						<c:if test="${page > 0 }">
							<li><a href="/FuMarket/?page=${page-1}">❮</a></li>
						</c:if>
						<c:if test="${page*12 <totalProduct }">
							<li><a href="/FuMarket/?page=${page+1}">❯</a></li>
						</c:if>
					</c:if>


				</ul>
			</div>

		</div>
	</div>

	<%@ include file="footer.jsp"%>
</body>
</html>