<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<head>
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqyjobZKV_bef9qMkQw69brKBHJO8Xtho&libraries=places"
	type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<link rel="stylesheet" href="<c:url value="/view/bootstrap.min.css"/>">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script type="text/javascript"
	src="<c:url value="/view/bootstrap-4.0.0-dist/js/bootstrap.min.js"/>"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/view/style.css"/>">
<link rel="icon" href="<c:url value="/view/media/Icons/homepage.png" />"
	type="image/x-icon">
</head>
<title>Chi tiết sản phẩm</title>
<script>
                $(document).ready(function(){
                    $(`#comment`).click(
                    function() {
                        $.ajax({
                            type: "POST",
                            url: '/FuMarket/getRating/${p.productID}',
                            data: {
                            	content: $("#rate-comment").val(),
                            },
                            success: function(data)
                            {
                            	console.log(data);
                            		if (data == "SUCCESS"){
                                		$(`#success`).text("Gửi thành công, cảm ơn ý kiến góp ý của bạn");
                                		
                            		}else {
                            			$(`#success`).text("Chưa gửi được");
                                	
                            		}

                            }
                        });
                    });
                });               
            </script>
</head>

<body>

	<%@ include file="header.jsp"%>


	<!--Begin container -->
	<div class="container" style="margin-top: 5px;">
		<!-- Category -->

		<div class="row">
			<div class="col-md-4" id="product-image">
				<!-- this is products image -->
				<!-- <img src="media/phpThumb_generated_thumbnail.png" alt="Bò húc lậu" class="image-responsive"> -->

				<c:forEach var="img" items="${imgs}">
					<img class="mySlides" id="${img.ID}"
						src="<c:url value="${img.URL}"/>" style="width: 100%"
						alt="${p.productName}">
				</c:forEach>

				<div class="w3-row-padding w3-section">

					<c:forEach var="img" items="${imgs}" varStatus="loop">
						<div class="w3-col s4">
							<img class="demo w3-opacity w3-hover-opacity-off"
								src="<c:url value="${img.URL}"/>" style="width: 100%"
								onclick="currentDiv(${loop.index})">
						</div>
					</c:forEach>
				</div>
				<!-- The Modal -->
				<div id="myModalImage" class="showModal">
					<span class="close-display-image">&times;</span> <img
						class="modal-content-image" id="img01">
					<div id="caption"></div>
				</div>

			</div>
			<div class="col-md-8">
				<div class="product-info">
					<!-- this is products info -->
					<h1 class="product-name">${p.productName}</h1>
					<h1 class="product-price">${p.price}</h1>
					<h5>
						Người bán: <span class="user-name">${p.fullName }</span>
					</h5>
					<h5>
						Tình trạng đơn hàng: <span class="product-quantity">Còn
							${p.quantity} </span>
					</h5>
					<h5>
						Doanh mục: <span class="category">${p.categoryName }</span>
					</h5>
					<h5>
						Rating: <span class="product-rate">${p.rate } sao</span>
					</h5>
					<div class="quantity">
						Số lượng:
						<button type="button" id="sub" class="sub">-</button>

						<input type="number" id="1" value="1" min="1" max="${p.quantity }" />
						<button type="button" id="add" class="add">+</button>
						<c:if test="${empty USER }">
							<a
								onclick="document.getElementById('notLogin').style.display='block'"><button
									type="button" class="bought">Mua</button></a>
						</c:if>
						<c:if test="${not empty USER }">
							<a
								onclick="document.getElementById('confirmChangeRoom').style.display='block'"><button
									type="button" class="bought">Mua</button></a>
						</c:if>

						<div id="notLogin" class="modal">
							<div class="modal-content animate" action="/action_page.php">
								<br>
								<center>
									<span style="margin: 30px 5px 30px 20px;"> <img
										src="<c:url value="/view/media/Icons/icons8-high-priority-48.png"/>"
										height="30px" width="30px">
									</span><span style="font-size: 20px;">Bạn chưa đăng nhập vui
										lòng đăng nhập để mua sản phẩm</span><br>

									<button type="submit" class="submitToLogin btn btn-success"
										style="font-size: 20px"
										onclick="document.getElementById('notLogin').style.display='none'">OK</button>
								</center>
								<button type="button"
									onclick="document.getElementById('notLogin').style.display='none'"
									class="cancelbtn">❌</button>

							</div>
						</div>

						<div id="confirmChangeRoom" class="modal">
							<form style="padding-top: 20px" class="modal-content animate"
								action="/action_page.php">
								<span
									style="margin-left: 30px; margin-top: 20px; font-size: 20px;">Sản
									phẩm đã mua <span style="color: red; font-size: 20px;">${p.productName}</span>
								</span><br> <span
									style="margin-left: 30px; margin-top: 20px; font-size: 20px;">Số
									lượng <span style="color: red; font-size: 20px;">${p.quantity}</span>
								</span><br> <span
									style="margin-left: 30px; margin-top: 20px; font-size: 20px;">Số
									phòng của bạn là<span style="color: red;margin-left: 10px; margin-top: 20px; font-size: 20px;">${u.address }</span>
								</span><br> <span
									style="margin-left: 30px; margin-top: 20px; font-size: 20px;"><img
									src="<c:url value="/view/media/Icons/caution.png"/>"
									height="30px" width="30px">Bạn có muốn thay đổi phòng
									không ?</span><br> <i style="margin-left: 30px; color: red">Nếu
									có vui lòng điền vào bên dưới</i><br> <input type="text"
									class="text-control" style="width: 80%; margin-left: 30px;"><br>
								<i style="margin-left: 30px; color: red">Nếu không vui lòng
									bỏ qua</i><br>
								<button type="button" onclick="addProductToCart(${p.productID},'${p.fullName}',${p.price},'${p.productName}')" class="btn btn-success"
									style="margin-left: 30px; margin-right: 50px">Thêm vào
									giỏi hàng</button>
								<button type="button" class="btn btn-info">Ship luôn</button>

								<button type="button"
									onclick="document.getElementById('confirmChangeRoom').style.display='none'"
									class="cancelbtn">❌</button>
							</form>
						</div>
					</div>
					<h5>
						Mô tả:
						<h5 class="description">${p.description }</h5>
					</h5>

				</div>

			</div>
		</div>
		
		<script>
			function addProductToCart(productID,sellerName,price,productName)
			{
				let quantity = $(`#quantity`).val();
				$.ajax({
					type : "POST",
					url : '/FuMarket/addProductToCart',
					data : {
						productID : productID,
						quantity : quantity,
						sellerName: sellerName,
						productName : productName,
						price : price
					},
					success : function(data) {
						console.log(data);
						if (data == "SUCCESS") {
							window.location.reload();
						} else {
							
						}
					}
				});
			}
		</script>
		<!-- end row -->
		<!-- comment-product -->
		<a href="#rating-products">
			<button type="button" class="btn btn-default" id="comment-product">Đánh
				giá sản phẩm</button>
		</a>
		<hr>

		<!-- begin relative-product-->
		<div class="relative-name">Sản phẩm liên quan:</div>
		<div class="relative-product">

			<c:forEach var="rel" items="${rel}">
				<div class="col-sm-2 cach">
					<div class="card">
						<img class="card-img-top" id="img-relative"
							src="/FuMarket/${rel.imageURL }" alt="" style="width: 100%;">
						<div class="card-body">
							<h5 class="card-title" style="font-weight: bold;">${rel.productName}</h5>
							<h4>${rel.price}</h4>
							<a href="/FuMarket/ProductDetail/${rel.productID }"
								class="btn btn-primary">Xem chi tiết</a>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>

		<!-- end relative-product -->
	</div>
	<!-- End container -->

	<!-- Begin Rate product -->
	<br>
	<c:if test="${not empty USER }">
		<div class="container" id="rating-products" style="padding: 0px;">
			<hr>
			<div class="rating-product">Đánh giá sản phẩm:</div>
			<div
				style="border-radius: 5px; background-color: aquamarine; margin: 10px; display: block; padding: 20px;">
				<div class="row">
					<div class="col-md-12" style="padding-left: 0px;">
						<div style="float: left; margin-left: 0px;">
							<p style="font-size: 20px">1. Đánh giá của bạn về sản phẩm
								này:</p>
						</div>
						<div style="margin-top: -10px;" class="rate">
							<input type="radio" id="star5" name="rate" value="5" /><label
								for="star5" title="5 sao">5 stars</label> <input type="radio"
								id="star4" name="rate" value="4" /><label for="star4"
								title="4 sao">4 stars</label> <input type="radio" id="star3"
								name="rate" value="3" /><label for="star3" title="3 sao">3
								stars</label> <input type="radio" id="star2" name="rate" value="2" /><label
								for="star2" title="2 sao">2 stars</label> <input type="radio"
								id="star1" name="rate" value="1" /><label for="star1"
								title="1 sao">1 star</label>
						</div>
					</div>
				</div>
				<div style="padding-bottom: 50px;">
					<p style="font-size: 20px;">2. Vui lòng viết xét của bạn vào
						bên dưới:</p>
						<textarea id="rate-comment" class="textfeedback"
							class="form-control" style="text-indent: 10px;" rows="15"
							cols="120%" autofocus
							placeholder="Viết vào đây nội dung bạn muốn đánh giá về sản phẩm..."
							required></textarea>
						<br>
						<button id="comment" type="submit" style="background-color: blue;"
							style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
							<strong>Gửi</strong>
						</button>
						<span id="success" style="color: red"></span>
				</div>
			</div>
		</div>
	</c:if>

	<!-- End product -->

	<!-- footer -->
	<%@ include file="footer.jsp"%>

	<script>
        var slideIndex = 1;
        showDivs(slideIndex);

        function plusDivs(n) {
            showDivs(slideIndex += n);
        }

        function currentDiv(n) {
            showDivs(slideIndex = n+1);
        }

        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            if (n > x.length) { slideIndex = 1 }
            if (n < 1) { slideIndex = x.length }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" w3-opacity-off", "");
            }
            x[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " w3-opacity-off";
        }

        //end slide image prodcut

        $('.add').click(function () {
            if ($(this).prev().val() < ${p.quantity}) {
                $(this).prev().val(+$(this).prev().val() + 1);
            }
        });
        $('.sub').click(function () {
            if ($(this).next().val() > 1) {
                if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
            }
        });
        // end quantity product

        //DISPLAY Image
        // Get the modal
        var modal = document.getElementById('myModalImage');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        //If have more image
        var arrImg = ['myImg1', 'myImg2', "myImg3"];

        for (let i = 0; i < arrImg.length; i++) {
            var img = document.getElementById(arrImg[i]);
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function () {
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close-display-image")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }
        }
         //end display image
         
         //scroll to top when page loaded
         
    </script>
	<script type="text/javascript">
	 $(document).ready(function(){
 		$(this).scrollTop(0);
		});      
   
      $('a[href*=#]:not([href=#])').click(function() {
     	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
     	        || location.hostname == this.hostname) {

     	        var target = $(this.hash);
     	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
     	           if (target.length) {
     	             $('html,body').animate({
     	                 scrollTop: target.offset().top
     	            }, 1000);
     	            return false;
     	        }
     	    }
     	});
	</script>

</body>

</html>