<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <head>
         <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqyjobZKV_bef9qMkQw69brKBHJO8Xtho&libraries=places"
            type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link rel="stylesheet" href="<c:url value="view/bootstrap.min.css"/>"> 

        <script type="text/javascript" src="<c:url value="bootstrap-4.0.0-dist/js/bootstrap.min.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<c:url value="view/style.css"/>">
        <link rel="icon" href="<c:url value="view/media/Icons/homepage.png" />"  type="image/x-icon">

        <script>
            $(function () {

                var // Define maximum number of files.
                    max_file_number = 3,
                    // Define your form id or class or just tag.
                    $form = $('form'),
                    // Define your upload field class or id or tag.
                    $file_upload = $('#uploadImage', $form),
                    // Define your submit class or id or tag.
                    $button = $('.submit', $form);

                // Disable submit button on page ready.
                $button.prop('disabled', 'disabled');

                $file_upload.on('change', function () {
                    var number_of_images = $(this)[0].files.length;
                    if (number_of_images > max_file_number || number_of_images < 3) {
                        alert(`Bạn phải tải lên đúng ${max_file_number} ảnh.`);
                        $(this).val('');
                        $button.prop('disabled', 'disabled');
                    } else {
                        $button.prop('disabled', false);
                    }
                });
            }); 
        </script>
    </head>

    <title>Thêm mặt hàng</title>
</head>

<body>


    <div class="menubar container-fluid">
        <nav class="navbar">
            <form class="navbar-form navbar-left" action="order.jsp">
                <div class="input-group form-group">
                    <input type="text" name="id" class="form-control" placeholder="Enter your order ID">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-info1 fa fa-search"></button>
                    </div>
                </div>
            </form>
            <ul class="nav navbar-nav">
                <li>
                    <a href="" class="nav-link">
                        <span class="fa fa-bell-o"> </span>&nbspThông báo
                    </a>
                </li>
                <li>
                    <a class="nav-link " onclick="myFunction()">
                        <span class="fa fa-address-book-o"> </span>&nbspĐăng nhập
                    </a>
                </li>
                <li>
                    <a href="/FuMarket/feedback" class="nav-link">
                        <span class="fa fa-commenting"> </span>&nbspFeedback
                    </a>
                </li>
                <li>
                    <a href="" class="nav-link">
                        <span class="fa fa-address-book"> </span>&nbspĐăng ký
                    </a>
                </li>

            </ul>
        </nav>
    </div>
    <!-- Header -->
    <div class="container">
        <div class="form-group">
            <form id="addProductForm" action="processUploadProduct" enctype="multipart/form-data" method="POST">
                <table style="line-height: 60px;">
                    <tr>
                        <td style="display: inline;margin-right: 20px;">
                            <span>1. Chọn ảnh (Chọn đúng 3 ảnh): </span>
                        </td>
                        <td style="line-height: 0px;">
                            <br>
                            <input id="uploadImage" type="file" class="form-control-file" name="images" accept="image/*" required multiple="multiple">
                        </td>
                    </tr>
                    <tr>
                        <td style="display: inline;margin-right: 20px;">
                            <span>2. Tên mặt hàng: </span>

                        </td>
                        <td> <input type="text" name="productName" class="form-control" id="addProductName" placeholder="VD: Cơm rang rưa bò"
                                required>
                    </tr>
                    <tr>
                        <td style="display: inline;margin-right: 20px;">
                            <span>3. Giá tiền:</span>

                        </td>
                        <td><input style="width: auto;margin-top: 10px;" class="form-control" id="addPrice" style="line-height:20px"
                                type="number" min="1000" max="20000000" step="500" name="price" id="priceProduct" required="required"
                                placeholder="(VNĐ)">

                        </td>

                    </tr>
                    <tr>
                        <td style="display: inline;margin-right: 20px;">
                            <span>4. Số lượng:</span>

                        </td>
                        <td><input style="width: auto;margin-top: 10px;" class="form-control" id="addNumber" style="line-height:20px"
                                type="number" min="1" max="100" step="1" name="quantity" id="priceProduct" required="required">

                        </td>

                    </tr>
                    <tr>
                        <td style="display: inline;margin-right: 20px;">
                            <span>5. Danh mục sản phẩm:</span>

                        </td>
                        <td>
                            <select style="width: auto" class="form-control" name="category" id="adCategory">
                                <option value="1">Đồ uống nhanh</option>
                                <option value="2">Đồ ăn vặt</option>
                                <option value="3">Quần áo</option>
                                <option value="4">Đồ công nghệ</option>
                                <option value="5">Dịch vụ giặt là</option>
                                <option value="6">Ship nước</option>
                                <option value="7">Thẻ điện thoại</option>
                                <option value="8">Đặt cơm</option>
                            </select></td>

                    </tr>
                    <tr>
                        <td style="display: inline;margin-right: 20px;">
                            <span>6. Mô tả sản phẩm:</span>

                        </td>
                        <td><textarea style="margin-top: 20px" name="description" class="form-control" id="addDescription" rows="5" cols="100"
                                placeholder="Mô tả sản phẩm của bạn..."></textarea>
                        </td>

                    </tr>
                    <tr>
                        <td style="line-height: 30px;">
                            <button style="width: 130px" type="submit" class="btn btn-primary mb-2">Thêm mặt hàng</button>
                        </td>
                    </tr>
                </table>
                <br>



            </form>
        </div>
    </div>



    <!-- footer -->
 <footer id="myFooter">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h2 class="logo">
                    <a href="/FuMarket">
                        <img class="Logo5" src="<c:url value="view/media/Logo.jpg" />" alt="">
                    </a>
                </h2>
            </div>
            <div class="col-sm-3">
                <h5>5 Chú ve sầu</h5>
                <ul>
                    <li>
                        <a href="https://www.facebook.com/beovuive1" target="_blank">Nguyễn Quang Trường</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/t0ank55" target="_blank">Bùi Anh Tú</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/buithanhtung.317" target="_blank">Bùi Thanh Tùng</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/dovan.tuan.777" target="_blank">Đỗ Văn Tuấn</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/dangkhoaydl" target="_blank">Tạ Đăng Khoa</a>
                    </li>

                    <li>
                        <a href="https://www.facebook.com/tuananh2209" target="_blank">Mentor: Nguyễn Tiến Tuấn Anh</a>
                    </li>

                </ul>
            </div>
            <div class="col-sm-2">
                <h5>About</h5>
                <div>

                    <p>Idea</p>

                </div>

                <div>

                    <p>Our open</p>
                </div>

                <div>

                    <p>
                        Soure project
                    </p>
                </div>

            </div>
            <div class="col-sm-2">
                <h5>License</h5>
                <ul>
                    <li>
                        <a href="project.html">Home</a>
                    </li>

                    <li>
                        <a href="gallery.html">Safety</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-2">
                <div class="social-networks">
                    <a href="" class="twitter">
                        <i class="">
                            <img src="<c:url value="view/media/icons8-facebook-48.png" />" alt="" width="30px" height="30px">
                        </i>
                    </a>
                    <a href="" class="facebook">
                        <i class="">
                            <img src="<c:url value="view/media/icons8-twitter-64.png" />" alt="" width="30px" height="30px">
                        </i>
                    </a>
                    <a href="" class="youtube">
                        <i class="">
                            <img src="<c:url value="view/media/youtube.png" />" alt="" width="30px" height="30px">
                        </i>
                    </a>
                </div>
                <a href="FUMarket/feedback">
                    <button type="button" class="btn btn-default">Feedback</button>
                </a>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <p>©2018 Bản quyền thuộc về Team 5 chú ve sầu </p>
    </div>

</footer>


</body>

</html>