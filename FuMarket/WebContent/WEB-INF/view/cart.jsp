<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqyjobZKV_bef9qMkQw69brKBHJO8Xtho&libraries=places"
            type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link rel="stylesheet" href="<c:url value="view/bootstrap.min.css"/>"> 

        <script type="text/javascript" src="<c:url value="bootstrap-4.0.0-dist/js/bootstrap.min.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<c:url value="view/style.css"/>">
        <link rel="icon" href="<c:url value="view/media/Icons/homepage.png" />"  type="image/x-icon">
    </head>

    <title>Giỏ hàng</title>
</head>


<body>
    <div class="menubar container-fluid">
        <nav class="navbar">
            <form class="navbar-form navbar-left" action="order.jsp" style="z-index: 0;">
                <div class="input-group form-group">
                    <input type="text" name="id" class="form-control" placeholder="Enter your order ID">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-info1 fa fa-search"></button>
                    </div>
                </div>
            </form>
            <ul class="nav navbar-nav">
                <li>
                    <a href="" class="nav-link">
                        <span class="fa fa-bell-o"> </span>&nbspThông báo
                    </a>
                </li>
                
                <li>
                    <a href="Fumarket/feedback" class="nav-link">
                        <span class="fa fa-commenting"> </span>&nbspFeedback
                    </a>
                </li>
                <li>
                    <a class="nav-link" onclick="document.getElementById('id01').style.display='block'">
                        <span class="fa fa-address-book-o"> </span>&nbspĐăng nhập
                    </a>
                </li>
                <li>
                    <a class="nav-link" onclick="document.getElementById('id02').style.display='block'">
                        <span class="fa fa-address-book"> </span>&nbspĐăng ký
                    </a>
                </li>
            </ul>
        </nav>
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="category-product">
            Danh mục sản phẩm ☰
        </button>
    </div>
    <!-- Header -->
    <div class="container" style="padding-bottom: 20px;">
        <h1 class="text-center m-t-25 m-b-25">Giỏ hàng của bạn</h1>
        <div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th> </th>
                        <th scope="col"> </th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="product" items="${ cart}" varStatus="loop">
                	<tr>
                        <th scope="row">${loop.index+1}</th>
                        <td>
                            ${product.productName }
                            <p><span>Người bán:</span> <span style="color:blue">${product.sellerName } </span> </p>
                            <button class="btn btn-danger">Xóa</button>
                        </td>
                        <td>
                            <div class="quantity">
                                <input type="number" id="1" value="${product.quantity }" min="1" max="8" />
                            </div>
                        </td>
                        <td>
                            <h3>${product.price * product.quantity} VND</h3>
                        </td>
                    </tr>
                
                </c:forEach>
                    
           
                </tbody>
            </table>
            <div>
                <button class="btn btn-success">Tiến hành ship hàng </button>
            </div>
            <div class="row" style="height:50px">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <ul class="pagination" style="margin:8px; float: right;">
                        <li>
                            <a href="#">❮</a>
                        </li>
                        <li>
                            <a href="#">❯</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>



    </div>


  <!-- footer -->
 <footer id="myFooter">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h2 class="logo">
                    <a href="/FuMarket">
                        <img class="Logo5" src="<c:url value="view/media/Logo.jpg" />" alt="">
                    </a>
                </h2>
            </div>
            <div class="col-sm-3">
                <h5>5 Chú ve sầu</h5>
                <ul>
                    <li>
                        <a href="https://www.facebook.com/beovuive1" target="_blank">Nguyễn Quang Trường</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/t0ank55" target="_blank">Bùi Anh Tú</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/buithanhtung.317" target="_blank">Bùi Thanh Tùng</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/dovan.tuan.777" target="_blank">Đỗ Văn Tuấn</a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/dangkhoaydl" target="_blank">Tạ Đăng Khoa</a>
                    </li>

                    <li>
                        <a href="https://www.facebook.com/tuananh2209" target="_blank">Mentor: Nguyễn Tiến Tuấn Anh</a>
                    </li>

                </ul>
            </div>
            <div class="col-sm-2">
                <h5>About</h5>
                <div>

                    <p>Idea</p>

                </div>

                <div>

                    <p>Our open</p>
                </div>

                <div>

                    <p>
                        Soure project
                    </p>
                </div>

            </div>
            <div class="col-sm-2">
                <h5>License</h5>
                <ul>
                    <li>
                        <a href="project.html">Home</a>
                    </li>

                    <li>
                        <a href="gallery.html">Safety</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-2">
                <div class="social-networks">
                    <a href="" class="twitter">
                        <i class="">
                            <img src="<c:url value="view/media/icons8-facebook-48.png" />" alt="" width="30px" height="30px">
                        </i>
                    </a>
                    <a href="" class="facebook">
                        <i class="">
                            <img src="<c:url value="view/media/icons8-twitter-64.png" />" alt="" width="30px" height="30px">
                        </i>
                    </a>
                    <a href="" class="youtube">
                        <i class="">
                            <img src="<c:url value="view/media/youtube.png" />" alt="" width="30px" height="30px">
                        </i>
                    </a>
                </div>
                <a href="FUMarket/feedback">
                    <button type="button" class="btn btn-default">Feedback</button>
                </a>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <p>©2018 Bản quyền thuộc về Team 5 chú ve sầu </p>
    </div>

</footer>

    <script src="main.js"></script>
</body>

</html>