<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqyjobZKV_bef9qMkQw69brKBHJO8Xtho&libraries=places"
            type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link rel="stylesheet" href="<c:url value="view/bootstrap.min.css"/>"> 

        <script type="text/javascript" src="<c:url value="bootstrap-4.0.0-dist/js/bootstrap.min.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<c:url value="view/style.css"/>">
        <link rel="icon" href="<c:url value="view/media/Icons/homepage.png" />"  type="image/x-icon">
    </head>

    <title>Quản lý Website - dành cho Admin</title>
    <script>
	$(document).ready(function() {
		$(`#btnLogin`).click(function() {

			$.ajax({
				type : "POST",
				url : '/FuMarket/processLogin',
				data : {
					email : $("#email").val(),
					password : $("#password").val()
				},
				success : function(data) {
					console.log(data);
					if (data == "SUCCESS") {
						window.location.replace('/FuMarket/');
					} else {
						$(`#errorMessage`).text("Wrong username or password");
						$(`#errorMessage`).css("display", "block");
					}
				}
			});
		});
	});
</script>
</head>

<body>




	<div class="menubar container-fluid">
		<nav class="navbar">
			<form class="navbar-form navbar-left" action="order.jsp"
				style="z-index: 0;">
				<div class="input-group form-group">
					<input type="text" name="id" class="form-control"
						placeholder="Enter your order ID" required="required">
					<div class="input-group-btn">
						<button type="submit" class="btn btn-info1 fa fa-search"></button>
					</div>
				</div>
			</form>
			<ul class="nav navbar-nav">
				<li><a href="" class="nav-link"> <span class="fa fa-bell-o">
					</span>&nbspThông báo
				</a></li>

				<li><a href="/FuMarket/feedback" class="nav-link"> <span
						class="fa fa-commenting"> </span>&nbspFeedback
				</a></li>

				<c:if test="${empty USER }">
					<li><a class="nav-link"
						onclick="document.getElementById('id01').style.display='block'">
							<span class="fa fa-address-book-o"> </span>&nbspĐăng nhập
					</a></li>
					<li><a class="nav-link"
						onclick="document.getElementById('id02').style.display='block'">
							<span class="fa fa-address-book"> </span>&nbspĐăng ký
					</a></li>
				</c:if>

				<c:if test="${not empty USER }">
					<li><a href="Toiban" class="nav-link"> <span
							class="fa fa-bell-o"> </span>&nbspTôi bán
					</a></li>
					<li><a href="buyHistory" class="nav-link"> <span
							class="fa fa-bell-o"> </span>&nbspLịch sử mua hàng
					</a></li>
					<li><a class="nav-link"
						onclick="document.getElementById('id03').style.display='block'">
							<span class="fa fa-address-book-o"> </span>&nbsp${USER.fullName}
					</a></li>
				</c:if>
			</ul>
		</nav>
		<!-- <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="category-product">
            Danh mục sản phẩm ☰
        </button> -->
	</div>

	<!-- Header -->
	<div id="id01" class="modal">
		<div class="modal-content animate" action="#">
			<div class="container1">
				<h2 class="testsign">Đăng nhập FU Market</h2>
				<label for="uname"> <b>Username</b>
				</label> <input type="text" id="email" placeholder="Enter Username"
					name="uname" required> <label for="psw"> <b>Password</b>
				</label> <input type="password" id="password" placeholder="Enter Password"
					name="psw" required>
				<div id="errorMessage" style="display: none; color: red;"></div>
				<button id="btnLogin">Login</button>
				<label> <input type="checkbox" checked="checked"
					name="remember"> Remember me
				</label> <label style="float: right; margin-top: 23px;"> <b>Forgot
						<a href="#"> password?</a>
				</b>
				</label>

				<p></p>
				<button class="btn btn-facebook" style="float: left;">
					<i class="fa fa-facebook">&nbsp</i> Đăng nhập bằng facebook
				</button>
				<button class="btn btn-google" style="float: right;">
					<i class="fa fa-google">&nbsp</i> Đăng nhập bằng Gmail
				</button>
				<button type="button"
					onclick="document.getElementById('id01').style.display='none'"
					class="cancelbtn">❌</button>
			</div>
		</div>
	</div>

	<div id="id02" class="modal">
		<form class="modal-content animate" action="processRegister" method="POST">
			<div class="container1">

				<h2 class="testsign">Đăng ký FU Market</h2>
				<label for="email"> <b>Email</b>
				</label> <input type="email" placeholder="EX: abc@xyz.com" name="email"
					required> <label for="name"> <b>Full name</b>
				</label> <input type="text" placeholder="VD: Nguyễn Văn A" name="name"
					required> <label for="psw"> <b>Password</b>
				</label> <input type="password" placeholder="Enter Password" name="psw"
					required> <label for="repsw"> <b>Retype
						Password</b>
				</label> <input type="password" placeholder="Retype Password" name="repsw"
					required> <label for="phone"> <b>Số điện thoại</b>
				</label> <input type="text" placeholder="VD: 1234******" name="phone"
					required> <label for="gender"> <b>Giới tính</b>
				</label> <input type="radio" name="gender" value="Male"> Nam <input
					type="radio" name="gender" value="Female" checked="checked"> Nữ
				<p></p>
				<label for="address"> <b>Phòng</b>
				</label> <input type="text" placeholder="VD: D113" name="address" required>
				<label> <input type="checkbox" checked="checked" required="required"
					name="accept"> Tôi đồng ý với điều khoản và chính sách của
					Hola Market
				</label>
				<button type="submit">Đăng ký</button>
				<p></p>
				<button type="button"
					onclick="document.getElementById('id02').style.display='none'"
					class="cancelbtn">❌</button>
			</div>
		</form>
	</div>

	<div id="id03" class="modal">
		<div class="modal-content animate">
			<div class="container1">
				<h2 class="testsign">Tài khoản FU Merket của bạn</h2>
				<label for="uname"> <b>Username</b>
				</label> <input type="text" id="email" value="${USER.fullName}"
					name="uname" required> <label for="psw"> <b>Password</b>
				</label> <input type="password" id="password" value="${USER.password}"
					name="psw" required> 	
				<a href="logout" style="color: white;"><button type="button" class="btn btn-danger" style="color: white;">Đăng xuất</a>
				<a style="color: white;"><button type="button" class="btn btn-info" style="float: right;" onclick="document.getElementById('updateProfile').style.display='block'">Cập nhật thông tin</a>
				<button type="button"
					onclick="document.getElementById('id03').style.display='none'"
					class="cancelbtn">❌</button>
			</div>
		</div>
	</div>

	<div id="updateProfile" class="modal">
		<form class="modal-content animate" action="processUpdateProfile" method="post">
			<div class="container1">

				<h2 class="testsign">Cập nhật thông tin tài khoản FU Market</h2>
				<label for="email"> <b>Email</b>
				</label> <input type="email" value="${USER.email}" name="email"
					required> <label for="name"> <b>Full name</b>
				</label> <input type="text" value="${USER.fullName}" name="name"
					required> <label for="psw"> <b>Password</b>
				</label> <input type="password" value="${USER.password}" name="psw"
					required> <label for="repsw"> <b>Retype
						Password</b>
				</label> <input type="password" value="${USER.password}" name="repsw"
					required> <label for="phone"> <b>Số điện thoại</b>
				</label> <input type="text" value="${USER.phone}" name="phone"
					required> <label for="gender"> <b>Giới tính</b>
				</label> <input type="radio" name="gender" value="Male"> Nam <input
					type="radio" name="gender" value="Female"> Nữ
				<p></p>
				<label for="address"> <b>Phòng</b>
				</label> <input type="text" value="${USER.address}" name="address" required>
				<label> <input type="checkbox" checked="checked" required="required"
					name="accept"> Tôi chắc chắn muốn cập nhật thông tin của mình
				</label>
				<button type="submit">Gửi yêu cầu</button>
				<p></p>
				<button type="button"
					onclick="document.getElementById('updateProfile').style.display='none'"
					class="cancelbtn">❌</button>
			</div>
		</form>
	</div>
	

    <!-- Code here -->
    <center>
        <div class="containerHomepageAdmin">
            <div class="titleHomepageForAdmin">
                <h2>Bạn đã đăng nhập tài khoản dành cho Admin</h2>
            </div>
            <br>
            <span style="font-size: 20px;color: rgb(255, 255, 255);">Chọn tùy chọn bên dưới</span>
            <br><br>
            <a style="color: aliceblue" href="manageProductForAdmin.html"> <button id="mP" type="button" class="btn btn-success">Quản
                    lý sản phẩm</button></a>
            <a style="color: aliceblue" href="manageUserForAdmin.html"><button id="mU" type="button" class="btn btn-success">Quản
                    lý người dùng</button>
        </div></a>
    </center>



    <!-- footer -->
    <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h2 class="logo">
                        <a href="/FuMarket">
                            <img class="Logo5" src="<c:url value="view/media/Logo.jpg"/>" alt="">
                        </a>
                    </h2>
                </div>
                <div class="col-sm-3">
                    <h5>5 Chú ve sầu</h5>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/beovuive1" target="_blank">Nguyễn Quang Trường</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/t0ank55" target="_blank">Bùi Anh Tú</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/buithanhtung.317" target="_blank">Bùi Thanh Tùng</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/dovan.tuan.777" target="_blank">Đỗ Văn Tuấn</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/dangkhoaydl" target="_blank">Tạ Đăng Khoa</a>
                        </li>

                        <li>
                            <a href="https://www.facebook.com/tuananh2209" target="_blank">Mentor: Nguyễn Tiến Tuấn Anh</a>
                        </li>

                    </ul>
                </div>
                <div class="col-sm-2">
                    <h5>About</h5>
                    <div>

                        <p>Idea</p>

                    </div>

                    <div>

                        <p>Our open</p>
                    </div>

                    <div>

                        <p>
                            Soure project
                        </p>
                    </div>

                </div>
                <div class="col-sm-2">
                    <h5>License</h5>
                    <ul>
                        <li>
                            <a href="project.html">Home</a>
                        </li>

                        <li>
                            <a href="gallery.html">Safety</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <div class="social-networks">
                        <a href="" class="twitter">
                            <i class="">
                                <img src="<c:url value="view/media/icons8-facebook-48.png"/>" alt="" width="30px" height="30px">
                            </i>
                        </a>
                        <a href="" class="facebook">
                            <i class="">
                                <img src="<c:url value="view/media/icons8-twitter-64.png"/>" alt="" width="30px" height="30px">
                            </i>
                        </a>
                        <a href="" class="youtube">
                            <i class="">
                                <img src="<c:url value="view/media/youtube.png"/>" alt="" width="30px" height="30px">
                            </i>
                        </a>
                    </div>
                    <a href="FUMarket/feedback">
                        <button type="button" class="btn btn-default">Feedback</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <p>©2018 Bản quyền thuộc về Team 5 chú ve sầu </p>
        </div>

    </footer>


    <script src="<c:url value="main.js"/>"></script>
</body>

</html>