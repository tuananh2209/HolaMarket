<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

	<div class="menubar container-fluid">
		<nav class="navbar">
			<a class="navbar-brand" href="http://localhost:8080/FuMarket/" style="border: none; color: white">FuMarket</a>
			<form class="navbar-form navbar-left" action="order.jsp"
				style="z-index: 0;">
				<div class="input-group form-group">
					<input type="text" name="id" class="form-control"
						placeholder="Enter your order ID" required="required">
					<div class="input-group-btn">
						<button type="submit" class="btn btn-info1 fa fa-search"></button>
					</div>
				</div>
			</form>
			<ul class="nav navbar-nav">
				<li><a href="" class="nav-link"> <span class="fa fa-bell-o">
					</span>&nbspThông báo
				</a></li>

				<li><a href="feedback" class="nav-link"> <span
						class="fa fa-commenting"> </span>&nbspFeedback
				</a></li>

				<c:if test="${empty USER }">
					<li><a class="nav-link"
						onclick="document.getElementById('id01').style.display='block'">
							<span class="fa fa-address-book-o"> </span>&nbspĐăng nhập
					</a></li>
					<li><a class="nav-link"
						onclick="document.getElementById('id02').style.display='block'">
							<span class="fa fa-address-book"> </span>&nbspĐăng ký
					</a></li>
				</c:if>

				<c:if test="${not empty USER }">
					<li><a href="Toiban" class="nav-link"> <span
							class="fa fa-bell-o"> </span>&nbspTôi bán
					</a></li>
					<li><a href="buyHistory" class="nav-link"> <span
							class="fa fa-bell-o"> </span>&nbspLịch sử mua hàng
					</a></li>
					<li><a href="cart" class="nav-link"> <span
							class="fa fa-bell-o"> </span>Cart
					</a></li>
					<li><a class="nav-link"
						onclick="document.getElementById('id03').style.display='block'">
							<span class="fa fa-address-book-o"> </span>&nbsp${USER.fullName}
					</a></li>
				</c:if>
			</ul>
		</nav>
		<!-- <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="category-product">
            Danh mục sản phẩm ☰
        </button> -->
	</div>

	<!-- Header -->
	<div id="id01" class="modal">
		<div class="modal-content animate" action="#">
			<div class="container1">
				<h2 class="testsign">Đăng nhập FU Market</h2>
				<label for="uname"> <b>Username</b>
				</label> <input type="text" id="email" placeholder="Enter Username"
					name="uname" required> <label for="psw"> <b>Password</b>
				</label> <input type="password" id="password" placeholder="Enter Password"
					name="psw" required>
				<div id="errorMessage" style="display: none; color: red;"></div>
				<button id="btnLogin">Login</button>
				<label> <input type="checkbox" checked="checked"
					name="remember"> Remember me
				</label> <label style="float: right; margin-top: 23px;"> <b>Forgot
						<a href="#"> password?</a>
				</b>
				</label>

				<p></p>
				<button class="btn btn-facebook" style="float: left;">
					<i class="fa fa-facebook">&nbsp</i> Đăng nhập bằng facebook
				</button>
				<button class="btn btn-google" style="float: right;">
					<i class="fa fa-google">&nbsp</i> Đăng nhập bằng Gmail
				</button>
				<button type="button"
					onclick="document.getElementById('id01').style.display='none'"
					class="cancelbtn">❌</button>
			</div>
		</div>
	</div>

	<div id="id02" class="modal">
		<form class="modal-content animate" action="processRegister" method="POST">
			<div class="container1">

				<h2 class="testsign">Đăng ký FU Market</h2>
				<label for="email"> <b>Email</b>
				</label> <input type="email" placeholder="EX: abc@xyz.com" name="email"
					required> <label for="name"> <b>Full name</b>
				</label> <input type="text" placeholder="VD: Nguyễn Văn A" name="name"
					required> <label for="psw"> <b>Password</b>
				</label> <input type="password" placeholder="Enter Password" name="psw"
					required> <label for="repsw"> <b>Retype
						Password</b>
				</label> <input type="password" placeholder="Retype Password" name="repsw"
					required> <label for="phone"> <b>Số điện thoại</b>
				</label> <input type="text" placeholder="VD: 1234******" name="phone"
					required> <label for="gender"> <b>Giới tính</b>
				</label> <input type="radio" name="gender" value="Male"> Nam <input
					type="radio" name="gender" value="Female" checked="checked"> Nữ
				<p></p>
				<label for="address"> <b>Phòng</b>
				</label> <input type="text" placeholder="VD: D113" name="address" required>
				<label> <input type="checkbox" checked="checked" required="required"
					name="accept"> Tôi đồng ý với điều khoản và chính sách của
					Hola Market
				</label>
				<button type="submit">Đăng ký</button>
				<p></p>
				<button type="button"
					onclick="document.getElementById('id02').style.display='none'"
					class="cancelbtn">❌</button>
			</div>
		</form>
	</div>

	<div id="id03" class="modal">
		<div class="modal-content animate">
			<div class="container1">
				<h2 class="testsign">Tài khoản FU Merket của bạn</h2>
				<label for="uname"> <b>Username</b>
				</label> <input type="text" id="email" value="${USER.fullName}"
					name="uname" required> <label for="psw"> <b>Password</b>
				</label> <input type="password" id="password" value="${USER.password}"
					name="psw" required> 	
				<a href="logout" style="color: white;"><button type="button" class="btn btn-danger" style="color: white;">Đăng xuất</a>
				<a style="color: white;"><button type="button" class="btn btn-info" style="float: right;" onclick="document.getElementById('updateProfile').style.display='block'">Cập nhật thông tin</a>
				<button type="button"
					onclick="document.getElementById('id03').style.display='none'"
					class="cancelbtn">❌</button>
			</div>
		</div>
	</div>

	<div id="updateProfile" class="modal">
		<form class="modal-content animate" action="processUpdateProfile" method="post">
			<div class="container1">

				<h2 class="testsign">Cập nhật thông tin tài khoản FU Market</h2>
				<label for="email"> <b>Email</b>
				</label> <input type="email" value="${USER.email}" name="email"
					required> <label for="name"> <b>Full name</b>
				</label> <input type="text" value="${USER.fullName}" name="name"
					required> <label for="psw"> <b>Password</b>
				</label> <input type="password" value="${USER.password}" name="psw"
					required> <label for="repsw"> <b>Retype
						Password</b>
				</label> <input type="password" value="${USER.password}" name="repsw"
					required> <label for="phone"> <b>Số điện thoại</b>
				</label> <input type="text" value="${USER.phone}" name="phone"
					required> <label for="gender"> <b>Giới tính</b>
				</label> <input type="radio" name="gender" value="Male"> Nam <input
					type="radio" name="gender" value="Female"> Nữ
				<p></p>
				<label for="address"> <b>Phòng</b>
				</label> <input type="text" value="${USER.address}" name="address" required>
				<label> <input type="checkbox" checked="checked" required="required"
					name="accept"> Tôi chắc chắn muốn cập nhật thông tin của mình
				</label>
				<button type="submit">Gửi yêu cầu</button>
				<p></p>
				<button type="button"
					onclick="document.getElementById('updateProfile').style.display='none'"
					class="cancelbtn">❌</button>
			</div>
		</form>
	</div>
	

    <script>
	$(document).ready(function() {
		$(`#btnLogin`).click(function() {

			$.ajax({
				type : "POST",
				url : '/FuMarket/processLogin',
				data : {
					email : $("#email").val(),
					password : $("#password").val()
				},
				success : function(data) {
					console.log(data);
					if (data == "SUCCESS") {
						window.location.replace('/FuMarket/');
					} else {
						$(`#errorMessage`).text("Wrong username or password");
						$(`#errorMessage`).css("display", "block");
					}
				}
			});
		});
	});
</script>