<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
          <link rel="stylesheet" href="<c:url value="view/bootstrap.min.css"/>"> 

        <script type="text/javascript" src="<c:url value="bootstrap-4.0.0-dist/js/bootstrap.min.js"/>"></script>
        <link rel="stylesheet" type="text/css" href="<c:url value="view/style.css"/>">
        <link rel="icon" href="<c:url value="view/media/Icons/history.png" />"  type="image/x-icon">
    </head>
    <title>Lịch sử mua hàng</title>
</head>

<body>
    <div class="menubar container-fluid">
        <nav class="navbar">


            <form class="navbar-form navbar-left" action="order.jsp">
                <div class="input-group form-group">
                    <input type="text" name="id" class="form-control" placeholder="Enter your order ID">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-info1 fa fa-search"></button>
                    </div>
                </div>
            </form>
            <ul class="nav navbar-nav">
                <li>
                    <a href="" class="nav-link">
                        <span class="fa fa-bell-o"> </span>&nbspThông báo
                    </a>
                </li>
                <li>
                    <a class="nav-link " onclick="myFunction()">
                        <span class="fa fa-address-book-o"> </span>&nbspĐăng nhập
                    </a>
                </li>
                <li>
                    <a href="feedback.html" class="nav-link">
                        <span class="fa fa-commenting"> </span>&nbspFeedback
                    </a>
                </li>
                <li>
                    <a href="" class="nav-link">
                        <span class="fa fa-address-book"> </span>&nbspĐăng ký
                    </a>
                </li>

            </ul>
        </nav>
    </div>
    <!-- Header -->
    <div class="container" style="margin-top: 10px; padding-bottom: 10px">
        <div>
            <center>
                <h2>Lịch sử mua hàng</h2>
            </center>
        </div>
        <table class="table">
            <tr>
                <th>Tên sản phẩm</th>
                <th>Số lượng</th>
                <th>Trạng thái</th>
                <th>Hủy đơn</th>
            </tr>
            <tr>
                <td scope="col">Bò húc</td>
                <td scope="col">2</td>
                <td scope="col">Đang xét đơn</td>
                <td>
                    <button type="button" class="btn btn-danger btn-sm" style="width: 80px;">Hủy đơn</button>
                </td>
            </tr>
            <tr>
                <td scope="col">Bò húc</td>
                <td scope="col">2</td>
                <td scope="col">Đang xét đơn</td>
                <td>

                </td>
            </tr>
            <tr>
                <td scope="col">Bò húc</td>
                <td scope="col">2</td>
                <td scope="col">Đang xét đơn</td>
                <td>

                </td>
            </tr>

        </table>
    </div>
    <!-- footer -->
    <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <h2 class="logo">
                        <a href="home.html">
                            <img class="Logo5" src="<c:url value="view/media/Logo.jpg"/>" alt="">
                        </a>
                    </h2>
                </div>
                <div class="col-sm-3">
                    <h5>5 Chú ve sầu</h5>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/beovuive1" target="_blank">Nguyễn Quang Trường</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/t0ank55" target="_blank">Bùi Anh Tú</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/buithanhtung.317" target="_blank">Bùi Thanh Tùng</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/dovan.tuan.777" target="_blank">Đỗ Văn Tuấn</a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/dangkhoaydl" target="_blank">Tạ Đăng Khoa</a>
                        </li>

                        <li>
                            <a href="https://www.facebook.com/tuananh2209" target="_blank">Mentor: Nguyễn Tiến Tuấn Anh</a>
                        </li>

                    </ul>
                </div>
                <div class="col-sm-2">
                    <h5>About</h5>
                    <div>

                        <p>Idea</p>

                    </div>

                    <div>

                        <p>Our open</p>
                    </div>

                    <div>

                        <p>
                            Soure project
                        </p>
                    </div>

                </div>
                <div class="col-sm-2">
                    <h5>License</h5>
                    <ul>
                        <li>
                            <a href="project.html">Home</a>
                        </li>

                        <li>
                            <a href="gallery.html">Safety</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <div class="social-networks">
                        <a href="" class="twitter">
                            <i class="">
                                <img src="<c:url value="view/media/icons8-facebook-48.png"/>" alt="" width="30px" height="30px">
                            </i>
                        </a>
                        <a href="" class="facebook">
                            <i class="">
                                <img src="<c:url value="view/media/icons8-twitter-64.png"/>" alt="" width="30px" height="30px">
                            </i>
                        </a>
                        <a href="" class="youtube">
                            <i class="">
                                <img src="<c:url value="view/media/youtube.png"/>" alt="" width="30px" height="30px">
                            </i>
                        </a>
                    </div>
                    <a href="feedback.html">
                        <button type="button" class="btn btn-default">Feedback</button>
                    </a>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <p>©2018 Bản quyền thuộc về Team 5 chú ve sầu </p>
        </div>

    </footer>


</body>

</html>