USE [master]
GO
/****** Object:  Database [Coding_Project]    Script Date: 9/8/2018 4:10:28 PM ******/
CREATE DATABASE [CodingProject]
GO
USE [CodingProject]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 9/8/2018 4:10:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](100) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 9/8/2018 4:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[FeedbackID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[FeedBack] [nvarchar](500) NULL,
 CONSTRAINT [PK_Feedback] PRIMARY KEY CLUSTERED 
(
	[FeedbackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 9/8/2018 4:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[OrderDetailID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[OrderID] [int] NULL,
	[Quantity] [int] NULL,
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
(
	[OrderDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 9/8/2018 4:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Status] [int] NULL,
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[UserID] [int] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderStatus]    Script Date: 9/8/2018 4:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatus](
	[StatusID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 9/8/2018 4:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductName] [nvarchar](100) NULL,
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[Quantity] [int] NULL,
	[Price] [money] NULL,
	[Rate] [real] NULL,
	[UserID] [int] NULL,
	[CategoryID] [int] NULL,
	[Ban] [bit] NULL,
	[Active] [bit] NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rating]    Script Date: 9/8/2018 4:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rating](
	[Commend] [nvarchar](200) NULL,
	[Rate] [real] NULL,
	[UserID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
 CONSTRAINT [PK_Rating] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 9/8/2018 4:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleID] [int] NOT NULL,
	[NameRole] [nvarchar](20) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 9/8/2018 4:10:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[FullName] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Password] [nvarchar](50) NULL,
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Address] [nvarchar](100) NULL,
	[Gender] [nvarchar](10) NULL,
	[Phone] [nvarchar](20) NULL,
	[Role] [int] NULL,
	[Ban] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (1, N'Đặt cơm')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (2, N'Đồ uống')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (3, N'Đồ ăn vặt')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (4, N'Đồ công nghệ')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (5, N'Quần áo')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (6, N'Ship nước')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (7, N'Thẻ game')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (8, N'Thẻ điện thoại')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (9, N'Đồ dùng phòng ngủ')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (10, N'Đặt bánh sinh nhật')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (11, N'Dịch vụ giặt là')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Feedback] ON 

INSERT [dbo].[Feedback] ([FeedbackID], [UserID], [FeedBack]) VALUES (1, 19, N'Người ta gọi đây là "Truất"')
INSERT [dbo].[Feedback] ([FeedbackID], [UserID], [FeedBack]) VALUES (2, 20, N'Website hay quá, cảm ơn team 5 chú ve sầu')
INSERT [dbo].[Feedback] ([FeedbackID], [UserID], [FeedBack]) VALUES (3, 21, N'thích quá')
INSERT [dbo].[Feedback] ([FeedbackID], [UserID], [FeedBack]) VALUES (4, 23, N'Good ')
INSERT [dbo].[Feedback] ([FeedbackID], [UserID], [FeedBack]) VALUES (5, 24, N'Wow, beautiful web')
INSERT [dbo].[Feedback] ([FeedbackID], [UserID], [FeedBack]) VALUES (6, 26, N'Nhờ website mà mình bán hàng hết nhanh hơn rất nhiều')
INSERT [dbo].[Feedback] ([FeedbackID], [UserID], [FeedBack]) VALUES (7, 28, N'Thật không thể tin được')
SET IDENTITY_INSERT [dbo].[Feedback] OFF
SET IDENTITY_INSERT [dbo].[OrderDetail] ON 

INSERT [dbo].[OrderDetail] ([OrderDetailID], [ProductID], [OrderID], [Quantity]) VALUES (1, 1, 6, 4)
INSERT [dbo].[OrderDetail] ([OrderDetailID], [ProductID], [OrderID], [Quantity]) VALUES (2, 5, 4, 4)
INSERT [dbo].[OrderDetail] ([OrderDetailID], [ProductID], [OrderID], [Quantity]) VALUES (3, 2, 5, 2)
INSERT [dbo].[OrderDetail] ([OrderDetailID], [ProductID], [OrderID], [Quantity]) VALUES (4, 7, 7, 1)
INSERT [dbo].[OrderDetail] ([OrderDetailID], [ProductID], [OrderID], [Quantity]) VALUES (5, 24, 8, 10)
INSERT [dbo].[OrderDetail] ([OrderDetailID], [ProductID], [OrderID], [Quantity]) VALUES (6, 25, 6, 4)
INSERT [dbo].[OrderDetail] ([OrderDetailID], [ProductID], [OrderID], [Quantity]) VALUES (9, 4, 9, 1)
INSERT [dbo].[OrderDetail] ([OrderDetailID], [ProductID], [OrderID], [Quantity]) VALUES (10, 22, 4, 1)
SET IDENTITY_INSERT [dbo].[OrderDetail] OFF
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Status], [OrderID], [Date], [UserID], [Active]) VALUES (1, 4, CAST(N'2018-08-22' AS Date), 20, 1)
INSERT [dbo].[Orders] ([Status], [OrderID], [Date], [UserID], [Active]) VALUES (2, 5, CAST(N'2018-08-26' AS Date), 23, 1)
INSERT [dbo].[Orders] ([Status], [OrderID], [Date], [UserID], [Active]) VALUES (1, 6, CAST(N'2018-08-22' AS Date), 21, 1)
INSERT [dbo].[Orders] ([Status], [OrderID], [Date], [UserID], [Active]) VALUES (3, 7, CAST(N'2018-09-01' AS Date), 22, 0)
INSERT [dbo].[Orders] ([Status], [OrderID], [Date], [UserID], [Active]) VALUES (2, 8, CAST(N'2018-08-01' AS Date), 22, 1)
INSERT [dbo].[Orders] ([Status], [OrderID], [Date], [UserID], [Active]) VALUES (1, 9, CAST(N'2018-08-09' AS Date), 26, 1)
SET IDENTITY_INSERT [dbo].[Orders] OFF
INSERT [dbo].[OrderStatus] ([StatusID], [Name]) VALUES (1, N'Đang ship đi')
INSERT [dbo].[OrderStatus] ([StatusID], [Name]) VALUES (2, N'Đang xét đơn')
INSERT [dbo].[OrderStatus] ([StatusID], [Name]) VALUES (3, N'Hủy')
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Bò húc', 1, 20, 12000.0000, 4, 23, 2, 0, 1, N'Bò húc được nhập khẩu từ Thái Lan')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'String', 2, 23, 10000.0000, 3.5, 21, 2, 0, 1, N'String mát lạnh')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Laptop DELL', 4, 1, 13000000.0000, 5, 20, 4, 0, 1, N'CPU:	Intel Core i3 Skylake, 6006U, 2.00 GHz
RAM:	4 GB, DDR4 (2 khe), 2400 MHz
Ổ cứng:	HDD: 500 GB
Màn hình:	14 inch, HD (1366 x 768)
Card màn hình:	Card đồ họa tích hợp, Intel® HD Graphics 520
Cổng kết nối:	2 x USB 3.0, HDMI, LAN (RJ45), USB 2.0, VGA (D-Sub)
Hệ điều hành:	Windows 10 Home SL
Thiết kế:	Vỏ nhựa, PIN rời
Kích thước:	Dày 23.4 mm, 1.95 kg')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Cơm Nguyễn Tuấn', 5, 100, 25000.0000, 2.5, 28, 1, 0, 1, N'Món ăn ''sạch''')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Rèm cửa ', 6, 9, 60000.0000, 4, 29, 9, 0, 1, N'Được may bằng 100% polyester dày, bền chắc

Giúp chắn bớt ánh sáng chiếu vào nhà

Móc treo được điều chỉnh lên xuống, tiện lợi sử dụng')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Nước Tority (19L)', 7, 40, 15000.0000, 4, 23, 6, 0, 1, N'Nước sạch từ siêu thị')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'IPhone X 64GB', 8, 5, 20000000.0000, 5, 26, 4, 0, 1, N'Chính hãng, Nguyên seal, Mới 100%, Chưa Active

Miễn phí giao hàng toàn quốc

Thiết kế: Nguyên khối

Màn hình: Super AMOLED capacitive touchscreen, 5.8 inch HD

Camera Trước/Sau: 7MP/ 2 camera 12MP

CPU: Apple A11 Bionic 6 nhân

Bộ Nhớ: 64GB

RAM: 3GB

SIM: 1 Nano SIM

Tính năng: Chống nước, chống bụi, Face ID')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Máy Ảnh Canon 750D + Lens 18-55 IS STM', 13, 10, 12000000.0000, 4.5, 26, 4, 0, 1, N'Cảm biến: CMOS 24.2MP

Bộ xử lý hình ảnh: DIGIC 6

ISO: ISO 100 - ISO 6400

Hệ thống lấy nét: 19 điểm

Chụp ảnh liên tục: tối đa khoảng 5,0 ảnh/giây

Truyền hình ảnh nhanh với kết nối Wifi')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Áo Lệch Vai 1 Dây Marc - Xanh', 14, 2, 99000.0000, 4, 27, 5, 0, 1, N'Đường may chắc chắn và tỉ mỉ

Chất vải mềm, bền đẹp, mát, thấm hút mồ hôi

Gam màu trẻ trung thời thượng, dễ phối đồ

Thiết kế trẻ trung, năng động')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Áo Cổ Tròn Rút Dây Tay Lở Marc - Trắng', 16, 3, 236000.0000, 4, 29, 5, 0, 1, N'Size M,L,SKU	5958576912676
Thương hiệu	Marc Fashion
, Sản xuất tại:    Việt Nam
Model:   JH052018
,Chất liệu: Cotton')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Máy Cạo Râu Nam Philips S5070', 17, 1, 2000000.0000, 4.5, 19, 4, 0, 1, N'Hệ thống lưỡi cạo ComfortCut

Dùng không dây 40 phút/sạc 1 giờ

Đầu tỉa chính xác SmartClick')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Macbook Air 2017 MQD32 (13.3 inch)', 18, 1, 20109000.0000, 5, 23, 4, 0, 1, N'Chip: Intel Core i5 Dual-core 1.8 GHz

RAM: 8GB 1600 MHz LPDDR3

Ổ cứng: 128GB PCIe-Based Flash

Chipset đồ họa: Intel HD Graphics 6000

Màn hình: 13.3 inch (900 x 1440 pixels) LED-backlit IPS LCD

Hệ điều hành: macOS Sierra

Pin: Li-Po 54 Wh')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Tai Nghe Bluetooth Chụp Tai Sennheiser Momentum 2.0 ', 20, 1, 10809000.0000, 5, 20, 4, 0, 1, N'Thiết kế hiện đại, phong cách

Chất lượng âm thanh vượt trội

Phím điều chỉnh phát nhạc và microphone

Pin có dung lượng 600 mAh')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Thẻ game Garena 200000', 21, 3, 194000.0000, 4.5, 21, 7, 0, 1, N'Rẻ nhất Hola')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Thẻ điện thoại Viettel, Mobi,Vina 100000', 22, 10, 95000.0000, 4.5, 21, 8, 0, 1, N'Rẻ nhất Hola')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Bánh Qui Đồng Tiền ', 23, 8, 13000.0000, 4, 19, 3, 0, 1, N' Ngon ngon, giảm béo')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Bim Bim trẻ em', 24, 30, 2000.0000, 4.5, 24, 3, 0, 1, N'Trở về tuổi thơ')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Sữa vinamilk', 25, 60, 4000.0000, 4.5, 24, 3, 0, 1, N'Rất tốt cho sức khỏe')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Cơm Cami', 26, 100, 250000.0000, 3, 100, 1, 0, 1, N'Cơm sạch')
INSERT [dbo].[Products] ([ProductName], [ProductID], [Quantity], [Price], [Rate], [UserID], [CategoryID], [Ban], [Active], [Description]) VALUES (N'Bò húc', 28, 40, 12000.0000, 4, 100, 2, 0, 0, N'OK')
SET IDENTITY_INSERT [dbo].[Products] OFF
INSERT [dbo].[Rating] ([Commend], [Rate], [UserID], [ProductID]) VALUES (N'Giá hợp lí, ăn ngon', 4.5, 20, 25)
INSERT [dbo].[Rating] ([Commend], [Rate], [UserID], [ProductID]) VALUES (N'Ship nhanh', 4, 21, 1)
INSERT [dbo].[Rating] ([Commend], [Rate], [UserID], [ProductID]) VALUES (N'Hay quá, mình ở tầng 5, nếu ship nước từ canteen thì quá mệt. Cảm ơn nhiều', 5, 24, 7)
INSERT [dbo].[Rating] ([Commend], [Rate], [UserID], [ProductID]) VALUES (N'Dùng quá ngon', 5, 26, 4)
INSERT [dbo].[Rating] ([Commend], [Rate], [UserID], [ProductID]) VALUES (N'Ship quá chậm', 3.5, 26, 5)
INSERT [dbo].[Rating] ([Commend], [Rate], [UserID], [ProductID]) VALUES (N'Trả tiền thiếu', 4, 27, 6)
INSERT [dbo].[Rating] ([Commend], [Rate], [UserID], [ProductID]) VALUES (N'Ship nhanh', 4.5, 28, 2)
INSERT [dbo].[Role] ([RoleID], [NameRole]) VALUES (0, N'Admin')
INSERT [dbo].[Role] ([RoleID], [NameRole]) VALUES (1, N'User')
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Bui Thanh Tung', N'tungtim@gmail.com', N'tungtim', 19, N'C103', N'Male', N'123456789', 0, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Do Van Tuan', N'tuandvse05339@fpt.edu.vn', N'ahihi', 20, N'C508', N'Male', N'0868455987', 0, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Ngyen Van Cuong', N'Cuongnv@gmail.com', N'noname', 21, N'B406', N'Male', N'1346546446', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Bui Thi Xuan', N'Conbuomxuan@gmail.com', N'12341234', 22, N'A106', N'Female', N'1131346478', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Bui Anh Tu', N'tubeo@gmail.com', N'noname', 23, N'D113', N'Male', N'0984625432', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyen Van A', N'abc@gmail.com', N'abc@1234', 24, N'F103', N'Male', N'0945321569', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyen Van B', N'bac@gmail.com', N'noname', 26, N'A504', N'Male', N'0946666165', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Hoang Thi Mo', N'Mo@gmail.com', N'bietchetlien', 27, N'D306', N'Female', N'01654232487', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Tuấn', N'nguyentuan@fpt.edu.vn', N'nguyentuan', 28, N'Ở Ngoài', N'Male', N'0985236145', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Lưu Ly', N'Ly@fpt.edu.vn', N'lyrem', 29, N'C311', N'Female', N'0964123455', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phan Văn Đức', N'phanduc0908@gmail.com', N'pvd', 30, N'Trọ ngoài ', N'Male', N'0981131870', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Bùi Trung Hiếu', N'hieubthe130790@fpt.edu.vn', N'bth', 31, N'B410', N'Male', N'01643241728', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Vũ Hải Lâm', N'lamvhhe130764@fpt.edu.vn', N'vhl', 32, N'B409R', N'Male', N'0913872067', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trần Hữu Hoàng', N'hoangthhe130065@fpt.edu.vn', N'thh', 33, N'C103', N'Male', N'0962743734', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Lê Thị Minh Châu', N'chaultmhe130788@fpt.edu.vn
', N'ltmc', 34, N'C302', N'Female', N'0984562147', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Đăng Kiên', N'kienndhe130994@fpt.edu.vn', N'ndk', 38, N'B402', N'Male', N'01654239874', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Thái Bảo', N'baontse05601@fpt.edu.vn', N'1', 87, N'C504', N'Male', N'1653754567', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Văn Huy', N'huynvhe130799@fpt.edu.vn', N'1', 88, N'B410L', N'Male', N'1655545964', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phạm Hồng Sơn', N'sonphse04579@fpt.edu.vn', N'1', 89, N'Ở ngoài', N'Male', N'1648534271', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Vũ Hồng Quân', N'quanvhhe130299@fpt.edu.vn', N'1', 90, N'A410', N'Male', N'942141869', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Quang Linh', N'linhnqse05580@fpt.edu.vn', N'1', 91, N'D508', N'Male', N'1246819963', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Quang Trường', N'truongnqse05350@fpt.edu.vn', N'1', 92, N'D113', N'Male', N'916289175', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phạm Ngọc Hòa', N'Hoapnse05740@fpt.edu.vn', N'1', 93, N'D413', N'Male', N'968038714', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trần Anh Quân', N'quantahe130722@fpt.edu.vn', N'1', 94, N'A103R', N'Male', N'949018498', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Tiến Mạnh', N'manhnthe130478@fpt.edu.vn', N'1', 95, N'A103', N'Male', N'968429906', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Hoàng Văn Thắng', N'thanghvse04806@fpt.edu.vn', N'1', 96, N'C105', N'Male', N'963733142', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trần Văn Sơn', N'sontvse04903@fpt.edu.vn', N'1', 97, N'? tr?', N'Male', N'1644810356', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trịnh Thị Hà My', N'mytthhe130054@fpt.edu.vn', N'1', 98, N'F207', N'Female', N'1684631607', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trần Duy Quyền', N'QuyenTDHE130010@fpt.edu.vn', N'1', 99, N'A102', N'Male', N'949964520', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phạm Thanh Tâm', N'tampths130068@fpt.edu.vn', N'1', 100, N'B204R', N'Female', N'913205055', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Thị Ngọc Khánh', N'khanhntnhe130796@fpt.edu.vn', N'1', 101, N'0', N'Female', N'965941453', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'đàm sơn tùng', N'tungdshe130134@fpt.edu.vn', N'1', 102, N'a103', N'Male', N'1673507955', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Hoàng Huy Linh', N'linhhhhhe130885@fpt.edu.vn', N'1', 103, N'F504', N'Male', N'1628828010', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Vương Mạnh Linh', N'linhvmhe130187@fpt.edu.vn', N'1', 104, N'D311', N'Male', N'868840256', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trịnh Quốc Khánh', N'khanhtqhe130273@fpt.edu.vn', N'1', 105, N'A108L', N'Male', N'1643633499', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Ma Công Linh', N'linhmche130808@fpt.edu.vn', N'1', 106, N'A205R', N'Male', N'1658886392', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Khánh Huyền', N'huyenntkse05487@fpt.edu.vn', N'1', 107, N'C312', N'Female', N'1688786095', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Thị Thùy Linh', N'linhnttse05291@fpt.edu.vn', N'1', 108, N'C306', N'Female', N'1642313344', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Tuấn Khoa', N'khoantse04443@fpt.edu.vn', N'1', 109, N'Ở ngoài', N'Male', N'1629708096', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Cao Thị Trang', N'trangcthe130276@fpt.edu.vn', N'1', 110, N'D303', N'Female', N'962478038', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Lã Vũ Nguyên Anh', N'anhlvnha130096@fpt.edu.vn', N'1', 111, N'b109', N'Male', N'1652345559', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phạm Quốc Đức', N'poseidon121995@gmaiil.com', N'1', 112, N'c105', N'Male', N'1692321212', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Quang Huy', N'huynqhe130069@fpt.edu.vn', N'1', 113, N'A108R', N'Male', N'1205648299', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Lê Thiện Văn', N'vanlthe130820@fpt.edu.vn', N'1', 114, N'A403', N'Male', N'1675956599', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Bùi Thanh Tùng', N'tungbtse05661@fpt.edu.vn', N'1', 115, N'Ở nhà', N'Male', N'1654442626', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Thị Lan Anh', N'lananh4415@gmail.com', N'1', 116, N'A401', N'Female', N'936073784', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phạm Thị Kim Dung', N'DungPTKHE130896@fpt.edu.vn', N'1', 117, N'F213', N'Female', N'1678373618', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn thị lộc', N'locntha130130@fpt.edu.vn', N'1', 118, N'B208', N'Female', N'1699032874', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Khánh Nhật', N'nhatnkhe130798@fpt.edu.vn', N'1', 119, N'A103', N'Male', N'945865587', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Vũ Phương Thảo', N'thaovphe130635@fpt.edu.vn', N'1', 120, N'D308r', N'Female', N'989779802', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Tiến Mạnh', N'manhntse05450@fpt.edu.vn', N'1', 121, N'C406', N'Male', N'962622395', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Từ Khắc Hiếu ', N'hieutkse05647@fpt.edu.vn', N'1', 122, N'C513', N'Male', N'1647520088', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Hữu Dũng', N'dungnhse05388@fpt.edu.vn', N'1', 123, N'Ở ngoài', N'Male', N'1659263711', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Hồng Minh', N'minhnhse05414@fpt.edu.com', N'1', 124, N'D314', N'Female', N'1694534110', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Duy Nam', N'namndhe130590@fpt.edu.vn', N'1', 125, N'C413', N'Male', N'961010300', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phùng Trí Đức', N'ducptse05234@fpt.edu.vn', N'1', 126, N'C405', N'Male', N'919436297', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Mạnh Hiếu', N'hieunmse05459@fpt.edu.vn', N'1', 127, N'D314', N'Male', N'1689688989', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn MInh Hiếu', N'hieunmhe130143@fpt.edu.vn', N'1', 128, N'C502', N'Male', N'971069208', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Duy Quân', N'quanndhe130677@fpt.edu.vn', N'1', 129, N'B401L', N'Male', N'1628186778', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trần Quang Nhạt', N'nhattqse05439@fpt.edu.vn', N'1', 130, N'Ở ngoài', N'Male', N'1675132555', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Bùi Thị Thu Trang', N'trangbtthe130155@fpt.edu.vn', N'1', 131, N'Ở ngoài', N'Female', N'911462952', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Tiến Dũng', N'dungnthe130669@fpt.edu.vn', N'1', 132, N'B108R', N'Male', N'972763249', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phan Xuân Vũ', N'vupxse05940@fpt.edu.vn', N'1', 133, N'C202', N'Male', N'1698336834', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Đinh Lệ Quỳnh Hương', N'huongdlqhe130744@fpt.edu.vn', N'1', 134, N'B308', N'Female', N'1665968598', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Hà Thị Ngọc Lưu Ly', N'lyhtnlse05285@fpt.edu.vn', N'1', 135, N'C311', N'Female', N'962830680', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Hồ Khánh Vũ', N'vuhkse05427@fpt.edu.vn', N'1', 137, N'C105', N'Male', N'1652543111', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Lê Vũ Long', N'vulongvn98@gmail.com', N'1', 138, N'C311', N'Male', N'1678783158', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Võ Đình Phúc', N'phucvdse03945@fpt.edu.vn', N'1', 139, N'Ở ngoài', N'Male', N'1698253735', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyen Vo Huy', N'huynvse05226@fpt.edu.vn', N'1', 140, N'C501L', N'Male', N'1207910045', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phùng Khắc Thành', N'phungthanh172@gmail.com', N'1', 141, N'Nhà riêng', N'Male', N'1224231996', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phạm Thị Oanh', N'oanhptse04853@fpt.edu.vn', N'1', 142, N'Trọ ngoài', N'Female', N'962873142', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Huỳnh Thị Thu Thảo', N'thaohttsb01714@fpt.edu.vn', N'1', 143, N'F207L', N'Female', N'983109556', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phạm Thị Mai Hương', N'huongptmhe130039@fpt.edu.vn', N'1', 144, N'Ở nhà', N'Female', N'961623418', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Phương Linh', N'linhnpsb01977@fpt.edu.vn', N'1', 145, N'No Room', N'Female', N'963996393', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Anh Tiến', N'tiennahe130396@fpt.edu.vn', N'1', 146, N'A103L', N'Male', N'968017102', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trần Thị Hiền', N'hientthe130251@fpt.edu.vn', N'1', 147, N'D310', N'Female', N'1672826141', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Phạm Bích Phượng ', N'phuongpbha130079@fpt.edu.vn', N'1', 148, N'F213', N'Female', N'969443057', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Bá Duy', N'duynbse04748@fpt.edu.vn', N'1', 149, N'Không có', N'Male', N'948932829', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Tùng Dương', N'duongntse05966@fpt.edu.vn', N'1', 150, N'C106', N'Male', N'963967197', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Trần Quốc Việt', N'viettqse06178@fpt.edu.vn', N'1', 151, N'D111', N'Male', N'1662933033', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Ngụy Tôn Hùng', N'hungnthe130903@fpt.edu.vn', N'1', 152, N'D414', N'Male', N'969799869', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Đỗ Quang Hiệp', N'hiepdqse05627@fpt.edu.vn', N'1', 153, N'D413', N'Male', N'1649088188', 1, 0)
INSERT [dbo].[Users] ([FullName], [Email], [Password], [UserID], [Address], [Gender], [Phone], [Role], [Ban]) VALUES (N'Nguyễn Xuân Cường', N'cuongnxse05392@fpt.edu.vn', N'1', 154, N'D413L', N'Male', N'982825126', 1, 0)
SET IDENTITY_INSERT [dbo].[Users] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UK_Users_Email]    Script Date: 9/8/2018 4:10:29 PM ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [UK_Users_Email] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK_Feedback_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK_Feedback_UserID]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_OrderID] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_OrderID]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_ProductID] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_ProductID]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Status] FOREIGN KEY([Status])
REFERENCES [dbo].[OrderStatus] ([StatusID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Status]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_UserID]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_CategoryID] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_CategoryID]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_UserID]
GO
ALTER TABLE [dbo].[Rating]  WITH CHECK ADD  CONSTRAINT [FK_Rating_ProductID] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[Rating] CHECK CONSTRAINT [FK_Rating_ProductID]
GO
ALTER TABLE [dbo].[Rating]  WITH CHECK ADD  CONSTRAINT [FK_Rating_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Rating] CHECK CONSTRAINT [FK_Rating_UserID]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([RoleID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Role]
GO
USE [master]
GO
ALTER DATABASE [CPFinal] SET  READ_WRITE 
GO
